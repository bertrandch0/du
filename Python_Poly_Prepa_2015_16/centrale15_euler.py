import matplotlib.pyplot as plt
from math import pi


def euler(f, y0, z0, h, n) :
    y, z = y0, z0
    ys, zs = [y], [z]
    for i in range(n - 1) :
        fy = f(y)
        y, z  = y + z*h, z + fy*h
        ys.append(y)
        zs.append(z)
    return ys, zs


def phase(methode, f, y0, z0, h, n, no) :
    ys, zs = methode(f, y0, z0, h, n)
    plt.plot(ys, zs)
    plt.savefig('Graph' + str(no) + '.pdf')


def verlet(f, y0, z0, h, n) :
    y, z = y0, z0
    ys, zs = [y], [z]
    for i in range(n - 1) :
        fy     = f(y)
        y      = y + h*z + 0.5*h**2*fy
        fy_new = f(y)
        z      = z + 0.5*h*(fy + fy_new)
        ys.append(y)
        zs.append(z)
    return ys, zs
