def recherche_pt_fixe(a) :
    inf, sup = 0, len(a) - 1
    while sup > inf:
        milieu = (inf + sup) // 2
        if a[milieu] == milieu :
            return milieu
        if a[milieu] > milieu :
            sup = milieu
            print('sup' + str(sup))
        else :
            inf = milieu
            print('inf' + str(inf))
    if a[sup] == sup :
        return sup
    raise ValueError("Pas de point fixe")            
    
def rechercheMax(l):
    ### Recherche dichotomique du max de l, liste sans répétition en dents de scie###
    i=int(len(l)/2)
    bsup=len(l)
    binf=0
    trouve=False
    while not trouve and bsup-binf>1:
        if i==0 or i==len(l):
            trouve = True
        elif l[i]<=l[i+1]:
            binf=i
        else:
            bsup=i
        i = int((binf+bsup)/2)
    if l[bsup]>l[binf]:
        return (bsup,l[bsup])
    if l[bsup]<l[binf]:
        return (binf,l[binf]) 
