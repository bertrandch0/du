% Objectif Bac : IPv4 1h30


# La somme de contrôle de l'en-tête IPv4

Voici un extrait d'une capture de trame avec l'outil `wireshark`
:::{custom-style="postit"}
[Note]{custom-style="Wireshark"}
Wireshark est un outil libre et puissant d'analyse des réseaux informatiques. Il
est installable sur  toute machine mais ne sera sûrement  pas disponible sur les
postes  de votre  lycée.  Vous  pourrez cependant  l'utiliser  sur votre  réseau
personnel en toute sécurité.
:::

```
Frame 253: 169 bytes on wire (1352 bits), 169 bytes captured (1352 bits) on interface 0
  [...]
Ethernet II, Src: FreeboxS_a3:6c:dd (00:24:d4:a3:6c:dd), Dst: WistronI_89:22:a7 (3c:97:0e:89:22:a7)
    Destination: WistronI_89:22:a7 (3c:97:0e:89:22:a7)
    Source: FreeboxS_a3:6c:dd (00:24:d4:a3:6c:dd)
    Type: IPv4 (0x0800)
Internet Protocol Version 4, Src: 80.243.180.87, Dst: 192.168.0.25
    0100 .... = Version: 4
    .... 0101 = Header Length: 20 bytes (5)
    Differentiated Services Field: 0x00 (DSCP: CS0, ECN: Not-ECT)
    Total Length: 155
    Identification: 0x5bff (23551)
    Flags: 0x4000, Don't fragment
    Time to live: 52
    Protocol: TCP (6)
    Header checksum: 0x2452 [correct]
    [Header checksum status: Good]
    [Calculated Checksum: 0x2452]
    Source: 80.243.180.87
    Destination: 192.168.0.25
```

On voit que la trame est au format IPv4 (Internet Protocol Version 4) et utilise
des adresses  sur 32 bits.  On y lit l'adresse  de la source  (`80.243.180.87`) et
celle du  destinataire (`192.168.0.25`). L'adresse  du destinataire étant  dans la
plage `192.168.xxx.xxx`:  c'est donc une adresse  du réseau privé non  visible à
l'extérieur.


`Wireshark` nous donne la version `hexdump` du paquet, où chaque ligne contient 16 octets, est précédée du numéro du premier octet de la ligne (en hexadécimal) et suivie de la traduction de la ligne en ASCII: 


```
0000  00 24 d4 a3 6c dd 3c 97 0e 89 22 a7 08 00 45 00   .$..l.<..."...E.
0010  00 9b 5b ff 40 00 34 06 24 52 c0 a8 00 19 50 f3   ...V@.@.......P.
0020  b4 57 9e 98 00 50 0d 88 4e 7e 37 5e ec 5f 80 18   .W...P..N~7^._..
0030  05 a4 c6 b9 00 00 01 01 08 0a 01 a6 d5 d6 45 e4   ..............E.
0040  ca b0 00 85 32 91 9d 43 6d 6c 66 93 ef b7 32 94   ....2..Cmlf...2.
0050  dc 24 a2 71 8b b4 02 d9 17 e0 37 00 9b dc 8e 88   .$.q......7.....
0060  85 bd bb bd 26 0a 2d 2b 40 f7 bd f5 f6 5f ff 1a   ....&.-+@...._..
0070  68 e9 15 b3 af bf 4b 4a 57 0a bf 99 b3 68 55 64   h.....KJW....hUd
0080  ef 0e af ab 90 d8 ee b6 73 70 05 60 7a 2a f1 41   ........sp.`z*.A
0090  ed de 04 22 f3 71 5a 41 e4 17 3c f4 ea 4b a6 5d   ...".qZA..<..K.]
00a0  49 8e 7a 07 df 07 49 63 e1 9a 13 17 e4 4d 94 25   I.z...Ic.....M.%
00b0  ea 33 ea 49 9a ca 31 32 b5 8e c7 bf 57 49 59 24   .3.I..12....WIY$
00c0  60 b6 4b a2 16 d2 a2 a7 11                        `.K......
```




Ce  qui  nous intéresse  ici  est  la somme  de  contrôle  de l'en-tête  (header
checksum)  qui est  calculée  par l'émetteur  et le  destinataire  et permet  de
détecter dans certains cas si le message a été mal transmis.

Pour  effecteur  ce calcul,  il  faut  récupérer  l'en-tête avec  `wireshark`  en
cliquant sur la ligne commençant par `Internet Protocol Version 4`:

![wireshark](./IMG/wireshark.png)


Voici un exemple
au format hexadécimal par paquets de 16 bits (4 caractères hexadécimaux):

`4500 009b 5bff 4000 3406 2452 c0a8 0019 50f3 b457`

La somme de contrôle correspond au  sixième paquet (`2452`). Elle est obtenue en
faisant la  somme des  9 paquets  de 16  bits précédents  en excluant  le paquet
contenant la somme de contrôle.

Ici on obtient:

`4500 + 009b + 5bff + 4000 + 3406 + c0a8 + 0019 + 50f3 + b457 = 2dbab`

Comme le  nombre dépasse  la taille de  16 bits, on  additionne le  caractère de
poids fort (ici `2`) aux quatre plus faibles (`dbab`) et on obtient:

`2 + dbab = dbad`

On recommence éventuellement  si le nombre obtenu est de  taille supérieure à 16
bits.

On prend  ensuite le complément  à 1  de ce nombre  en passant par  son écriture
binaire:

`dbad -> 1101101110101101`

`        0010010001010010 -> 2452`

On obtient bien 2452 comme précisé dans l'en-tête (`Header checksum: 0x2452 [correct]`).


**Le but de cette partie est de  fabriquer une fonction Python extrayant un paquet
IPv4 et vérifiant la somme de contrôle.**


### tshark

On va récupérer un paquet IPv4 quelconque grâce à `tshark`, une version en ligne
de commande  de `wireshark`  qu'on peut installer  simplement, par  exemple sous
Debian/Ubuntu:

```bash
$ sudo apt-get install tshark
```

puis on entre:

```bash
$ sudo tshark -i2 -c1 -f 'ip' -x
```

et on obtient :

```bash
Capturing on 'enp0s25'
0000  3c 97 0e 89 22 a7 00 24 d4 a3 6c dd 08 00 45 00   <..."..$..l...E.
0010  00 34 1e b3 40 00 34 06 62 05 50 f3 b4 57 c0 a8   .4..@.4.b.P..W..
0020  00 19 00 50 9e 98 3b 2e e0 df 0d d7 4f dd 80 10   ...P..;.....O...
0030  bf 4a 3d 79 00 00 01 01 08 0a 46 53 c6 d0 01 c2   .J=y......FS....
0040  8c 5c                                             .\
1 packet captured
```

*  l'option   `-i2`  indique  que   l'on  choisit  la  seconde   interface  (ici
  `enp0s25`). On obtient la liste des interfaces avec `$ tshark -D`.
  
* l'option `-c1` indique que l'on ne capturera qu'un seul paquet.

* l'otion `-f  'ip'` indique qu'on filtre  le flux pour ne  sélectionner que les
  paquets correspondant au champ `ip` c'est-à-dire sur `tshark` `IPv4`.
  
* enfin  l'option `-x`  qu'on veut  obtenir le format  du paquet  en hexadécimal
  accompagnée de sa traduction en ASCII (format habituel des fichiers binaires).
  

Dans un premier temps, il s'agit d'extraire la partie codant l'en-tête: il s'agit
de la partie qui commence au quinzième octet, donc ici `45` : `4`
pour indiquer  la version  d'IP et  `5` qui  donne la  longueur de  l'en-tête en
nombre de mots de 32 bits.

1. D'après  ces renseignements, donnez  l'en-tête IPv4 qui correspond  au paquet
   capturé ci-dessus.
   

### cut et tr

On voudrait enlever la  premiere colonne du tableau qui numérote  les mots de 32
bits du paquet.

2.  Expliquez  les  valeurs  inscrites   dans  cette  colonne  (`0000`,  `0010`,
   `0020`,...).

3.  À l'aide  de la  commande système  `cut`, transformez  la sortie  donnée par
   `tshark` afin d'enlever les numéros de mots de 16 bits et la traduction ASCII
   afin d'obtenir en reprenant l'exemple :
   
   ```bash
3c 97 0e 89 22 a7 00 24 d4 a3 6c dd 08 00 45 00
00 34 1e b3 40 00 34 06 62 05 50 f3 b4 57 c0 a8
00 19 00 50 9e 98 3b 2e e0 df 0d d7 4f dd 80 10
bf 4a 3d 79 00 00 01 01 08 0a 46 53 c6 d0 01 c2
. Le 5 qui 8c 5c 
```

4. À  l'aide de la  commande système `tr`, écrivez  le dernier flux  obtenu sous
   forme d'une seule ligne sans espace : 
   
```bash
3c970e8922a70024d4a36cdd0800450000341eb340003406620550f3b457c0a8001900509e983b2ee0df0dd74fdd8010bf4a3d7900000101080a4653c6d001c28c5c
```

5. Sélectionnez maintenant l'en-tête IPv4 à l'aide de `cut`.


### Python

Pour varier les plaisirs, nous voudrions à présent récupérer les en-têtes et les
traiter avec Python. Pour cela, la bibliothèque `subprocess` de Python permet de
lancer  un processus  et de  récupérer  le flux  sortant dans  une variable.  La
syntaxe est la suivante :

```python
sortie = subprocess.check_output(commande, shell=True)
```

Commande est  ce que l'on  entrerait dans le terminal,  sous forme de  chaîne de
caractère.

Attention, `sortie`  est de  type `byte`  : il  faut la  convertir en  chaîne de
caractères pour la traiter avec les méthodes habituelles:


```python
import subprocess

cmd = "sudo tshark -i2 -c1 -f 'ip' -x | cut --delimiter=' ' -f 3-18 | tr -d '\n ' | cut -c 29-68"

octets = subprocess.check_output(cmd, shell=True)
```

Alors on observe :

```python
>>> octets
b'450000abb89340004006bbadc0a8001950f3b457\n'

>>> type(octets)
bytes
```
6. Créez une variable `entete` qui ne  contiendra que les 20 octets de l'en-tête
   sous forme de chaîne de caractères sans `b` ni `\n`.

7.  Faites la somme des mots de 32 bits qui forment
   l'en-tête comme expliqué dans le préambule. Vous devez calculer :
   
   `4500 + 00ab + b893 + ...`
   
8. Si  la somme s'écrit sur  plus de 2 octets,  il faut récupérer la  retenue et
   l'ajouter à la troncature de la somme  sur 2 octets jusqu'à obtenir un nombre
   sur  2 octets  (cf préambule).  Effectuez  cette transformation  à l'aide  de
   Python.
   
9.  Il reste  à  vérifier que  la  somme  de contrôle  est  encore correct  côté
   récepteur. Relisez  le pr'mbule qui  explique la  fabrication de la  somme de
   contrôle et imaginez un moyen simpleet astucieux d'effectuer cette vérification.
   
10. Proposez un script 100% `bash` faisant accomplissant la même tâche.

# La feuille de route

1. Les 4 premiers chiffres ne font  pas partie du paquet. Ensuite, chaque paquet
   de deux  chiffres en hexadécimal correspond  à un octet.  Le 5 qui suit  le 4
   indique  qu'il faut  considérer 5  mots de  32 bits  : combien  de bits  cela
   représente-t-il  ? Donc  combien  d'octets ?  Donc combien  de  paquets de  2
   chiffres ?
   
2.  Comment s'écrit  seize en  hexadécimal?  Combien y  a-t-il de  paquets de  2
   caractères par ligne ?

3. La  syntaxe de `cut`  qui nous intéresse ici  est `cut --delimiter=d  -f a-b`
   pour sélectionner les champs (**f**ields) du numéro `a` au numéro `b` inclus,
   ces champs  étant délimités  par le caractère  `d`. Attention !  Il y  a deux
   espaces entre les deux premières colonnes puis un seul entre les 16 suivantes
   puis à nouveau deux espaces.
   
4. La syntaxe de  `tr` est `tr -d chaîne` où `d`  signifie *delete*. Cela permet
   de  supprimer dans  le flux  courant  toutes les  occurrences des  caractères
   contenus dans `chaîne`. Le  passage à la ligne est codé  par `\n` (code ASCII
   10, cf fiche 6). 
   
5. La  syntaxe pour  sélectionner des  caractères repérés  par leurs  rangs avec
   `cut` est  `cut -c  a-b` pour  sélectionner les caractères  du numéro  `a` au
   numéro `b` (il n'y a maintenant qu'une seule ligne).
   
6. Observez  ce que donne  `str(octets)` et  ne sélectionner que  les caractères
   souhaités avec `[début:fin]`.
   
7. Il y a 10 mots de 32 bits dans l'en-tête. Pour transformer la chaîne `'4500'`
   en l'entier en base 16 correspondant, on peut le faire avec `eval('0x4500')`.
   Il s'agit  donc de parcourir les  dix paquets de la  liste, d'effectuer cette
   transformation puis de faire la somme.

8. L'usage de *masque*  est ici approprié en utilisant les  opérateurs bit à bit
   (cf  fiche 5).  Par  exemple,  effecteur l'opération  `n  &  0xff` permet  de
   sélectionner les huit bits de poids faible du nombre `n`. 
   Pour diviser un nombre par $2^k$ on peut utiliser l'opérateur `nombre >> k`.
   
9. En  relisant le préambule, on  s'aperçoit que le  complément à 1 de  la somme
   sans la *checksum*  est égal à la *checksum*. Si  l'on note $\overline{x}$ le
   complément à 1 d'un  nombre $x$, $c$ la *checksum* et $s$  la somme qui vient
   d'être  calculée   à  la  question   précédente,  alors  cela   signifie  que
   $\overline{s - c} = c$ donc que $\overline{s}=c+\overline{c}$. Que vaut donc $s$?

10. Il faut utiliser des accents graves pour récupérer le flux d'une commande:

```bash
entete=`sudo tshark -i3 -c1 -f 'ip' -x | cut --delimiter=' ' -f 3-18 | tr -d '\n ' | cut -c 29-68`
```

Pour découper l'en-tête et en faire la  somme, on peut utiliser `cut` t rajouter
`0x` devant le paquet de quatre caractères hexadécimaux :


```bash
$ chaine="deadcafe"
$ un=`echo "$chaine" | cut -c 1-4`
$ deux=`echo "$chaine" | cut -c 5-`
$ un="0x"$un
$ deux="0x"$deux
$ echo $((un deux+))
108971
```

En effet, on vérifie sur Python :

```Python
>>> 0xdead + 0xcafe
108971
```

