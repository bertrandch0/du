% Réseaux

### En Bref:
en  2019,  presque  30  milliards  d'objets sont  connectés  à  un  réseau  pour
communniquer :  quelles sont  les grandes lignes  des protocoles  qui permettent
d'organiser ces échanges à si grande échelle ?

## Réseau informatique

Un  réseau est  un système  composé d'éléments  matériels (contrôleur  ethernet,
fibre  optique,  modem,  routeur,...)  et  logiciels  (pilotes  des  interfaces,
firmares  des  équipements,...)  dont  la  fonction est  le  transport  de  flux
d'informations. Il  sert à  mettre en  oeuvre des  services (mise  à disposition
d'imprimantes, d'applications, amélioration des performances,...).

Les  contraintes   sont  nombreuses:   il  faut  transporter   n'importe  quelle
information, de  n'importe quelle  taille, n'importe  où et  concilier sécurité,
fiabilité,...

## Étendue, mode de fonctionnement et topologie

Les réseaux se différencient en particulier par:

*  leur taille:  PAN (Personnal  Area Network),  LAN (Local  Area Network),  MAN
  (Metropolitan Area Network), WAN (Wide Area Network)
* leur mode de fonctionnement: on peut envoyer un message à tous les équipements
  (broadcast), ou certains seulement (multicast).  Cela correspond aux réseaux de
  petite taille. Pour  les grands réseaux (internet), on communique  avec un seul
  équipement (unicast) via de nombreux supports. Il existe au moins un chemin
  entre deux équipements.
* leur topologie: on peut disposer les équipement en étoile, en bus, en maillage
  complet ou partiel,...


## Architecture en couches

On  découpe un  probléme (faire  communiquer  deux ordinateurs  par exemple)  en
sous-problèmes hiérarchiśes.  Pour les  réseaux, on  distingue des  *couches* de
différents  niveaux, des  couches de  même  niveau communiquant  entre elles  et
assurant un *service* pour les couches supérieures en
supposant que les services des couches inférieures sont assurées.


:::{custom-style="postit"}
[Note]{custom-style="Exemple"}
Par exemple, la communication entre deux chefs d'entreprises (qui s'échangent
des lettres) peut être décomposé en trois niveaux de communication: 

* les ingénieurs qui s'échangent des machines;
*  les secrétariats  qui récupèrent  les  machines, les  mettent dans  plusieurs
  emballages,  fournissent  un  plan  pour  reconstituer  la  machine,  trouvent
  l'adresse des autres secrétariats; 
* les bureaux de poste qui récupèrent  les paquets, les mettent dans des sacs
  postaux  et les  envoient au  bureau de  poste le  plus proche  du secrétariat
  destinataire  via un  réseau de  centres postaux  relayés par  trains, avions,
  camions, les chemins n'étant pas forcément uniques.
:::

Conceptuellement, le dialogue  entre couches de même niveau  est horizontal selon
un *protocole*, mais
en pratique il est vertical: il passe par l'utilisation des services des niveaux
inférieurs.

## Modèle OSI


### Généralités

Le modèle OSI (open Systems Interconnection) est une norme ISO permettant
d'organiser la  communication entre  des systèmes  informatiques. Il  contient 7
couches (le modèle TCP/IP vu en 2nde en contient 4).


### Résumé

|7 Application : interface utilsateur (exemples: smtp, http, ssh,...). |
|----------------|
|6 Presentation : assure que les données sont présentées sous un format acceptable (exemples: Ascii, Unicode, Jpeg,...), s'occupe de compresser, coder, décoder le message.|
|5 Session: décide d'établir ou de terminer la connexion, le "droit à la parole", la synchronisation, etc. (Sockets)|
|4 Transport : casse (ou reconstitue) le message en segments numérotés et vérifie la fiabilité de la transmission (TCP, UDP)|
|3 Réseau : s'occupe du routage des paquets, du trajet entre source et destination, donne une adresse logique (IP)|
|2 Liaison : s'occupe des adresses physiques (MAC) des trames au niveau local (LAN)|
|1 Physique : transmet de manière effective les bits (exemple: ethernet, wifi,...)|



### En détail

Couches 7, 6, 5 : Le message est au bon format, doit être utilisé pour une certaine
tâche et  doit bien  être transmis  vers un certain  destinataire qui  peut être
repérée par une adresse e-mail, http, ftp, etc.  

Couche 4. Le message est découpé en *segments* au niveau de la couche 4, chaque segment est
   numéroté  et  contient  en  plus  un   numéro  de  *port*  qui  correspond  à
   l'application qui l'utilise. Par exemple, le port 80 pour HTTP, 25 pour SMTP,
   etc. Au niveau  des couches 4 de  la source et de l'émetteur,  on vérifie si
   l'ordre est  respecté, si chaque  segment a été reçu,  quel est l'état  de la
   connexion  (en écoute,  en attente  de fermeture  après réception  du dernier
   segment, etc.) 

Couche 3. Les segments de la couche 4 sont encapsulés dans des *paquets* IP qui contiennent
   en plus des segments, les adresses logiques sous forme IP (fournies selon la position dans le réseau via le DHCP - *Dynamic Host Configuration Protocol* ) de 
   la source  et du  destinataire, la longueur  du paquet, sa  durée de  vie. On
   teste entre couches 3 de la source et du destinataire s'ils sont dans le même
   réseau. Dans le cas 
   contraire, la source au niveau de  cette couche devient un routeur (un relai)
   disposant d'une 
   table de routage lui permettant de définir la prochaine étape. 
:::{custom-style="postit"}
[Note]{custom-style="traceroute"}
   La commande shell `traceroute allblacks.com` permet par exemple de déterminer
   le chemin  pour aller de sa  machine à la  page de ce site  néo-zélandais. On
   obtient les adresses IP de 13  routeurs qu'on peut tracer géographiquement et
   voir que le message est passé par Nantes, Paris, Francfort, Paris à nouveau, Marseille,
   New-York, New-Delhi, Singapour...
   :::
   
Couche 2. Le message est traité en couche 2 si le dernier routeur et le destinataire
   sont dans  le même  réseau. Une  machine contient  une carte  réseau physique
   associée à une interface 
   réseau logique  qui lui  donne un nom  et une adresse  unique fournie  par le
   fabricant (adresse MAC - Media Access  Control). Le paquet est encapsulé dans
   une *trame* qui contient 
   l'adresse source et 
   l'adresse de destination à l'intérieur d'un réseau local (LAN) et est le plus
   souvent  transmis  à  tous  les  membres du  réseau  (broadcast).  Le  membre
   destinataire se fait connaître. Des collisions
   entre messages des différents membres sont possibles et sont gérées pour être
   évitées. En cas de non réception, décision de d'émettre à nouveau.
    Le signal peut être  affaibli, distordu, bruité. Il existe donc
   des moyens  de détecter  et corriger  les erreurs  de transmissions  (code de
   Hamming, principe du maximum de vraissemblance, codes polynomiaux,...).
   Il faut également  empêcher l'émetteur d'envoyer des  données plus rapidement
   que le récepteur ne peut les traiter : utilisation de tampons (buffers).

Couche 1.  Le message prêt à être transmis à ce niveau est fiable, dans l'ordre, sans répétitions. Il transite par ondes radios , par
   signaux électriques dans 
   un cable,etc. et peut être déformé en réception. Les couches supérieures vont alors effectuer des contrôles et des corrections.


   
   
:::{custom-style="postit"}
[Note]{custom-style="Remarque"}
Il faut  bien noter  que chaque  couche de  la source  ne communique  qu'avec la
couche de même niveau du récepteur.  En particulier, lors du transit des données
en dehors du LAN,  les données sont encapsulées et ne sont  pas lues. Seules les
adresses   sont  traitées.   Cependant,  cela   n'est  que   théorique  et   les
implémentations réelles ne suivent pas forcément à la lettre le modèle théorique.
:::
