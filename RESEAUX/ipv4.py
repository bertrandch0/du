import subprocess

cmd = "tshark -i1 -c1 -f 'ip' -x"

octets = subprocess.check_output(cmd, shell=True)

decoupe = str(octets).split('  ')

lignes = ''.join((' '.join([decoupe[i] for i in [1,3,5]])).split(' '))

ipv4 = lignes[28:68]


paquets4 = [ipv4[4*k:4*k+4] for k in range(len(ipv4)//4)]

cs = paquets4[5]

autres = paquets4[:5] + paquets4[6:]

somme = hex(sum([int(p, 16) for p in autres]))

reduit = hex(int(somme,16)//0x10000 + int(somme,16)%0x10000)

print(somme, reduit, cs)
