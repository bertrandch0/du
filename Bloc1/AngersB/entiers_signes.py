import numpy as np

a = np.int8(127)

b = np.int8(a + 2)

def i8(n):
    return np.int8(n)


def algo_diff(n:int) -> str:
    nb = n
    res = ''
    pow = 0
    while 2**pow <= nb:
        pow += 1
    pow2 = 2**(pow - 1)
    while pow2 >= 1 :
        test = int(pow2 <= nb)
        res += str(test)
        print(pow2, nb, test, res)
        nb -= test*pow2
        pow2 //= 2
    return res

def digit(n:int)->str:
    if 0 <= n < 10:
        return str(n)
    else:
        return chr(97 + (n - 10))

#digit = {k:str(k) if k < 10 else chr(87 + k) for k in range(36) }    

def base(b: int,n : int) -> str:
    assert 2 <= b < 36, 'La base doit être entre 2 et 36' 
    nb = n
    res = ''
    while nb >= 1:
        rem = nb % b
        res = digit(rem) + res
        nb //= b
    return res



def add_bin(b1: str, b2: str) -> str:
    t = max(len(b1), len(b2))
    b1 = '0'*(t - len(b1)) + b1
    b2 = '0'*(t - len(b2)) + b2
    ret = 0
    som = 0
    nb = ''
    for k in range(t-1,-1,-1):
        s = int(b1[k]) + int(b2[k]) + ret
        ret = s // 2
        nb = str(s % 2) + nb
    return nb
        

def comp1(n: str) -> str:
    c = ''
    for d in n:
        c = str(int(not(int(d)))) + c
    return c

def bin_signe(n:int) -> str:
    if n >= 0:
        return base(2, n)
    else:
        return add_bin(comp1(base(2,-n)),'1')


