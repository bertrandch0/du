# CODAGE DE L'INFORMATION - BLOC 1
Confinées, confinés, bonjour.

Nous allons ce matin essayer de travailler ensemble mais à distance respectueuse.

Nous tenterons d'aborder au moins 3 thèmes :

* la norme IEEE-754 -> c'est [ICI](IEE754.md) et [LÀ](IEE754_part2.md)
* un rapide survol du codage des caractères et un travail à faire sur l'UTF8 -> c'est [LÀ](https://gitlab.com/lyceeND/1ere/-/blob/master/2019_20/1_Archi_1/TD_CodageCarac.md)
* Un rappel de cours sur le codage des caractères au cas où et une présentation de la compression avec l'algorithme de Huffman le tout regroupé dans ce [PDF](https://gitlab.com/lyceeND/1ere/-/blob/master/2019_20/1_Archi_1/CodageCarac.pdf)
* une proposition de [réponse au transcodage UTF8](https://gitlab.com/lyceeND/1ere/-/blob/master/2019_20/1_Archi_1/defi4.py) à ne regarder qu'après moult recherches.
* Représentation de la musique : une proposition de travail sur les fichiers [MIDI](https://gitlab.com/lyceeND/1ere/-/blob/master/2019_20/3_Langage_Progammation_1/10-TP_Fonctions.md#mini-projet-musical-partie-2)
* Représentation des images : une proposition de découvertes des [images](https://gitlab.com/lyceeND/1ere/-/blob/master/2019_20/3_Langage_Progammation_1/10-TP_Fonctions.md#d%C3%A9couverte-des-images-et-des-matrices)

Si vous avez d'autres desiderata, n'hésitez pas à en faire part...

Guillaume
