from MaBase_MIB_dic import *
from typing import List,  Dict
from copy import deepcopy
import csv


Attribut = str
Valeur = str
Ligne = Dict[Attribut, Valeur]
Table = List[Ligne]


def vers_csv(nom_table: str, ordre_cols: List[Attribut]) -> None:
    """
    Exporte une liste de dictionnaires sous forme d'un
    fichier csv. On rentre le nom de la table sous forme de chaîne.
    On donne l'ordre des colonnes sous la forme d'une liste d'attributs.
    >>> vers_csv('Groupe1', ordre_cols=['Nom','Anglais','Info','Maths'])
    """
    with open(nom_table + '.csv', 'w') as fic_csv:
        ecrit = csv.DictWriter(fic_csv, fieldnames=ordre_cols)
        table = eval(nom_table)
        ecrit.writeheader() # pour le 1ère ligne
        for ligne in table:
            ecrit.writerow(ligne) # lignes ajoutées 1 à 1
    return None


def select(table: Table, critère: str) -> Table:
    def test(ligne: Ligne) -> bool:
        return eval(critère)
    return [ligne for ligne in table if test(ligne)]

def projection(table:Table, liste_clés:List[Attribut]) -> Table:
     return [{clé:ligne[clé] for clé in ligne if clé in liste_clés} for ligne in table]

def tri(table: Table, attribut: Attribut, decroit:bool =False) -> Table:
    def critère(ligne: Ligne) -> str:
        return ligne[attribut]
    return sorted(table, key=critère, reverse=decroit)


def jointure(table1: Table, table2: Table, cle1: Attribut, cle2: Attribut = None) -> Table:
    if cle2 is None: # Par défaut les clés de jointure portent le même nom
        cle2 = cle1
    new_table: Table = []  # La future table créée, vide au départ
    for ligne1 in table1: 
        for ligne2 in table2:
		    #  on ne  considère que  les lignes  où les  cellules de  l'attribut
		    # choisi sont identiques. 
            if ligne1[cle1] == ligne2[cle2]:
                new_line = deepcopy(ligne1) # on copie entièrement la ligne de table1 
                for cle in ligne2: # on copie la ligne de table2 sans répéter la
                                   # cellule de jointure
                    if cle != cle2:
                        new_line[cle] = ligne2[cle]
                new_table.append(new_line)
    return new_table
