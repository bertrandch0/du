# Bloc 2 

Dans ce module seront abordés les thèmes suivants :

1. (Re)découverte de la récursion : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/GiYoM%2Fdu/master?filepath=Bloc2%2F1-Recursion.ipynb)
2. Tri fusion : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/GiYoM%2Fdu/master?filepath=Bloc2%2F2-Tri_fusion.ipynb)
3. Diviser pour régner