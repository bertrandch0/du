def mulRusse2(x: int, y:int, acc: int = 0) -> int:
    if x == 0:
        return acc
    return mulRusse(x >> 1, y << 1, acc + (y & (-(x&1))))

def mulRusse(x: int, y: int, acc: int = 0) -> int:
    if x == 0:
        return acc
    return mulRusse2(x//2, y*2, acc + y*(x%2))




def expo1(x: float, n: int) -> float:
    return 1 if n == 0 else x*expo1(x, n - 1)

def expo1b(x: float, n: int) -> float:
    if n == 0:
        return 1
    return x*expo1b(x, n - 1)

def expo2(x: float, n: int) -> float:
    """x^(2n) = (x^n)*(x^n) """
    if n == 0:
        return 1
    elif n%2 == 0:
        return expo2(x, n//2)**2
    else:
        return x*expo2(x, n//2)**2 

def expo3(x: float, n: int) -> float:
    if n == 0:
        return 1
    return (1 + (x-1)*(n%2))*expo2(x, n//2)**2

def expo4(x: float, n: int) -> float:
    acc = 1
    exp = n
    fact = x
    while exp > 0:
        if exp%2 == 1:
            acc *= fact
        fact *= fact
        exp //= 2
    return acc

def expo5(x: float, n: int, acc: int = 1) -> float:
    if  n == 0:
        return acc
    print(f"x {x}, acc {acc}, n {n}")
    return expo5(x*x, n//2, acc*(1 + (x - 1)*(n%2)))

print(expo5(2,10))