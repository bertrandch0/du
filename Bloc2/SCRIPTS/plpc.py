from random import choices
from typing import List

def prefix_commun(s1: str, s2: str) -> str:
    """
    Renvoie le préfixe commun de deux chaînes
    """
    res = ""
    max_ind = min(len(s1), len(s2))
    i = 0
    while i < max_ind and s1[i] == s2[i]:
        res += s1[i]
        i += 1
    return res

def plus_long_prefix(ns: List[str]) -> str:
    def parcours(lo: int, hi: int) -> str:
        if lo == hi:
            return ns[lo]
        if lo < hi:
            mid = lo + (hi - lo)//2 # (lo + hi)//2
            ns1 = parcours(lo, mid)
            ns2 = parcours(mid + 1, hi)
            return prefix_commun(ns1, ns2)
    return parcours(0, len(ns) - 1)

ns = ['gatacca', 'gatatata', 'gatagagac', 'gata']
print(plus_long_prefix(ns))
