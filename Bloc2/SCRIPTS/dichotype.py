from typing import TypeVar, Sequence, List
from typing_extensions import Protocol
from abc import abstractmethod

# on utilise Sequence car on peut rechercher dans tout objet itérable dont les éléments sont accessibles par un indice
# pour définir un type générique d'éléments d'une liste, ces éléments devant être comparables
El = TypeVar('El', bound="Comparable")
Ind = int  # un type synonyme pour distinguer les indices des entiers quelconques


class Comparable(Protocol):
    """
    Les instances de Comparable ont juste la contrainte d'avoir une méthode lt (less than : < ) qui soit définie 
    """
    @abstractmethod
    def __lt__(self: El, other: El) -> bool:
        pass


L: List[int] = [1, 2, 4, 6, 7, 9, 10, 11, 14, 15]


def dicho(x: El, L: Sequence[El], lo: Ind = 0, hi: Ind = len(L)) -> bool:
    if lo == hi:
        return False
    c = (lo + hi) // 2
    if x == L[c]:
        return True
    if x < L[c]:
        return dicho(x, L, lo, c)
    else:
        return dicho(x, L, c, hi)


if __name__ == "__main__":
    print(dicho(1, L))

print(dicho(1, L))
