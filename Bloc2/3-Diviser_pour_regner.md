# διαίρει καὶ βασίλευε


## Algorithme de Karatsouba

* Addition de deux entiers de $`n`$ chiffres: $`O(n)`$

* Multiplier un entier de $`n`$ chiffres par un entier de 1 chiffre: $`O(n)`$
* Algorithme de l'école  primaire pour multiplier deux  nombres de
$`n`$ chiffres : $`n`$ multiplications d'un nombre de $`n`$ chiffres par un
nombre de 1 chiffre puis une addition des $`n`$ nombres obtenus: $`O(n^2)`$



>**Idée**  $`O(n^2)`$: multiplication  de nombres  deux fois  plus petits  $`⟶`$
>quatre fois plus rapide ?


$`xy=(10^mx_1+x_2)(10^my_1+y_2)=10^{2m}x_1y_1+10^m(x_2y_1+x_1y_2)+x_2y_2`$




```
Fonction MUL(x: entier, y:entier) -> entier
Si n==1 Alors
   Retourner x.y
Sinon
   m  <- ENT(n/2)
   x1 <- ENT(x/10^m)
   x2 <- x mod 10^m
   y1 <- ENT(y/10^m)
   y2 <- y mod 10^m
   a  <- MUL(x1, y1, m)
   b  <- MUL(x2, y1, m)
   c  <- MUL(x1, y2, m)
   d  <- MUL(x2, y2, m)
   Retourner a.10^2m + (b +c).10^m + d
FinSi
```




Les divisions et  les multiplications par des  puissances de 10 ne  sont que des
décalages effectués en temps constant.

L'addition finale est en $`\lambda n`$ donc le temps d'exécution est défini par:

$`T(n)=4T(\lfloor n/2\rfloor) +\lambda n \qquad T(1)=1`$



 $`n=2^k  \qquad T(n)=T(2^k)=x_k`$




$`x_k=4x_{k-1}+\lambda 2^k \qquad x_0=1`$

$`
 \begin{array}{rcl}
x_k&=&4(4x_{k-2}+\lambda' 2^{k-1})+\lambda 2^k\\
&=& 4^2x_{k-2} + (2\lambda' + \lambda)2^k\\
&=& 4^4x_{k-4}+(2^3\lambda'''+2^2\lambda''+2^1\lambda' +2^0\lambda)2^k\\
  &=&  4^kx_0+\Theta\left(\displaystyle\sum_{i=0}^{k-1}2^i\right)  2^k\\
&=&4^k+\Theta\left(2^k\times 2^k\right) \\
&=&n^2 + \Theta( n^2) \\
&=& Θ(n^2)    
  \end{array}
`$


```
:/
```

Moins formellement, sachant que la multiplication est en $`n^2`$, on divise ici la taille par 2 mais se retrouve avec 4 sous-problèmes, plus précisément 4 sous-problèmes indépendants en $`(n/2)^2=n^2/4`$ et $`4\times (n^2/4)=n^2`$ ce qui n'arrange pas nos affaires, ce qui fait dire au grand Kolmogorov en 1956 qu'on ne fera sûrement jamais mieux que du $`n^2`$.


Mais en 1960, le jeune Anatoli Karatsouba se souvient de ses cours de mathématiques de collège:  $`bc + ad = ac+bd -(a-b)(c-d)`$.

```
:)
```

**En quoi cela simplifie le problème? Quelle est alors la nouvelle complexité?**

$$`x_2y_1+x_1y_2=x_1y_1+x_2y_2-(x_1-x_2)(y_1-y_2)`$$

## Diviser pour régner: ze Master Theorem of divide and conquer algorithms


*Le commandement  du grand nombre  est le même pour  le petit nombre,  ce n'est qu'une question de division en groupes.*

*孙子兵法 VIe siècle avant JC*




On considère un problème  de taille n qu'on découpe en  a sous-problèmes de taille
n/b avec a et b des entiers. Le coût de l'algorithme est alors:

$`
\begin{cases}
  T(1) = 1\\
  T(n) = a×T(n/b) + \text{Reconstruction(n)}
\end{cases}
`$


En général la reconstruction est de l'ordre de $`c×n^α`$.



Par exemple, pour l'algorithme de КАРАЦУБА nous avions  
$`T(n)=3T(\lfloor n/2\rfloor)  +\Theta(n)`$
$`a=3`$, $`b=2`$, $`α=1`$ et *c* quelconque avec *n* pair.




$` \begin{array}{rcl}
T(n)&=&aT(n/b)+cn^α\\
&=&a^2T(n/b^2)+ac(n/b)^α+cn^α\\
&=&\cdots\\
&=&a^kT(n/b^k)+\displaystyle\sum_{i=0}^{k-1}a^ic(n/b^i)^α 
\end{array}
`$


$`n=b^{k_n}`$ 


$`T(n)=a^{k_n}+cn^α\displaystyle\sum_{i=0}^{k_n-1}(a/b^α)^i`$


$`T(n)=a^{k_n}+cn^α\displaystyle\sum_{i=0}^{k_n-1}(a/b^α)^i`$


*  $`a> b^\alpha`$  alors la  somme géométrique  est équivalente  au premier  terme
  négligé    à     une    constante    multiplicative    et     additive    près
  ($`\frac{1}{a/b^α-1}\left(\frac{a}{b^α}\right)^{k_n} -\frac{1}{a/b^α-1} `$ ) à savoir $`(a/b^α)^{k_n}`$ et $`T(n)=O(a^{k_n})`$ soit
  $`T(n)=O(n^{\log_b(a)})`$
* $`a=b^α`$ alors $`T(n)=O(n^α\log_b(n))`$
* $`a <  b^α`$ alors la série géométrique  est convergente: $`S(n)\simeq
  n^{\log_b(a)}+cn^α/(1-a/b^α)`$ or $`a<b^α`$ donc $`\log_b(a)<α`$. Finalement $`T(n)=O(n^α)`$
  
  
  


Pour revenir à l'algorithme de 
КАРАЦУБА
, on a $`a=3`$, $`b=2`$, $`\alpha = 1`$ donc $`b^α=2<a`$. D'après ce qui précède, on obtient donc que $$`T(n)=\Theta(n^{\log_23})\simeq \Theta(n^{1,58})`$$.
On ne va peut-être pas voir le problème ainsi en Terminale mais on peut garder l'idée maîtresse en tête.

## Diviser pour régner pour les nuls...enfin presque

On va pouvoir mettre au point un algorithme *diviser pour régner* lorsqu'on pourra résoudre un problème en trois étapes:

1. **Diviser**:  transformer un problème en sous-problèmes de taille plus petite (typiquement deux fois plus petite)
2. **Régner**: ici *conquérir* serait plus approprié car ce terme donne une vision plus dynamique du phénomène. On conquiert chaque sous-problème en appelant récursivement notre méthode de division sur des sous-problèmes de plus en plus petits jusqu'à devenir résolvables en temps constant.
3. **Reconstruire**: on combine tous les sous-problèmes résolus de proche en proche jusqu'à obtenir la solution du problème initial.
Les Babyloniens n'ont pas attendu d'avoir des ordinateurs et ont eu l'idée d'améliorer les recherches dans une liste si elle est déjà  triée avec ce que nous avons appelé la *recherche dichotomique*. 

On peut formaliser la méthode en *pseudo*-python:

```python
def DPR(Tab: liste, i: indice , j: indice) :
    if petit_probleme(Tab, i, j):
        return solution(Tab, i, j)
    else:
        spb1 = DPR(Tab, i, (i+j)//2)
        spb2 = DPR(Tab, (i+j)//2, j)
        return combine(spb1, spb2)
```

Cela s'applique bien au tri fusion. Pour Karatsouba, on a trois sous-problèmes, pour la recherche dichotomique, on élimine un des deux sous-problèmes.

Nous verrons au Bloc 5 un autre moyen de diviser un problème en sous-problèmes mystérieusement appelé *programmation dynamique*. Nous verrons que les champs d'action sont différents même si au premier abord le principe semble le même. Les algorithmes DPR sont efficaces quand les sous-problèmes sont indépendants et appelés une seule fois. Cette caractéristique rend particulièrement efficace une parallélisation de la résolution comme vous le verrez prochainement dans le chapitre sur les processus et le multi-threading (un tri fusion parallélisé sera proposé).
# Exercices


### Multiplication du paysan russe


Voici ce qu'apprenaient les petits soviétiques pour multiplier deux
entiers. Une variante de cet algorithme a été retrouvée sur le papyrus
de Rhind datant de 1650 avant JC, le scribe Ahmes affirmant que cet
algorithme était à l'époque vieux de 350 ans. Il a survécu en Europe
occidentale jusqu'aux travaux de Fibonacci.



```console
FONCTION MULRUSSE( x entier ,y: entier, acc:entier) → entier
   SI x == 0
      RETOURNΕ acc
   SINON
      SI x est pair
         RETOURNE MULRUSSE(x/2, y*2, acc)
      SINON
         RETOURNE MULRUSSE((x-1)/2, y*2, acc + y)
      FINSI
   FINSI
```


Que vaut `acc` au départ?

Écrivez une version récursive de cet algorithme en évitant l'alternative
des lignes 5 à 9.

Écrivez une version impérative de cet algorithme.

Prouvez la correction de cet algorithme.

Étudiez sa complexité.


En python, `x >> 1` décale l'entier *x* d'un bit vers la doite et `x << 1` décale *x* d'un
bit vers la gauche en complétant par un zéro à droite, `x & y` renvoie l'entier
obtenu en faisant la conjonction logique bit à bit des représentations
binaires de *x* et *y* et `~x` renvoie le complément à 1 de *x*.

Ré-écrivez la multiplication russe en n'utilisant que ces opérations bit à
bit (pas de division ni de multiplication).

## Exponentiation rapide

Comment calculer $`x^n`$ avec $`x`$ un nombre à virgule flottante et $`n`$ un entier naturel ? Quel serait un algorithme naïf ? Donnez-en une version impérative et une version récursive. Peut-on utiliser le principe de diviser pour régner ? 

## Plus long préfixe commun

Le travail sur les chaînes de caractère est à la mode dans le programme de Terminale. En voici un nouvel avatar : on a une série de chaînes (des chaînes de nucléotides ?) et on voudrait avoir la longueur des plus longs préfixes communs (les généticiens ont des raisons que la raison ne connait pas).

```python
>>> ns = ['gatacca', 'gatatata', 'gatagagac', 'gata']
>>> plus_long_prefix(ns)
gata
```


## Recherche du plus fort 

On suppose qu'on dispose de $`n`$ éléments distincts dans un tableau. La seule chose qu'on sait faire avec ces éléments consiste à dire si deux éléments sont égaux ou non. On dit qu'un élément est *majoritaire* s'il apparaît strictement plus que $`n/2`$ fois dans le tableau. Pour se simplifier la vie, on peut supposer au départ que $`n`$ est une puissance de 2.

1. Donner une fonction qui calcule le nombre d'occurrence d'un élément donné dans une liste Python. Déduisez-en un algorithme naïf qui vérifie l'existence d'un élément majoritaire. Quelle est sa complexité au pire ?
2. Déterminer une fonction récursive qui utilise le principe de diviser pour régner. Complexité ?
3. Et si $`n`$ n'est pas une puissance de 2?


## Tri rapide

Ce tri est un des plus célèbres et a été proposé en 1959 par Sir Tony (Hoare). L'idée est simple : on choisit un pivot, on place les plus petits que lui à sa gauche et les plus grands à sa droite et on continue récursivement jusqu'à obtenir une liste de longueur 1.
 

## Deux points les plus proches (pas facile...)

On a $`n`$ points dans le plan caractérisés par leurs coordonnées cartésiennes. On voudrait les deux plus proches. Commencez par de la force brute puis essayez de diviser pour régner...Vous pourrez par exemple considérer que le plan se divise en deux catégories : les points à gauche du point milieu (en considérant les points horizontalement) et les points à sa droite.

Vous pourrez lire [ce document](http://people.csail.mit.edu/indyk/6.838-old/handouts/lec17.pdf) pour vous aider. Ce qui est intéressant ici c'est que c'est la reconstruction qui est compliquée.

![close pair](./IMG/closepair.png)
