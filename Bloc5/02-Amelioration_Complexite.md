# Améliorer la complexité

## Exemple : exponentiation

On veut  calculer $`x^n`$ sachant  que *n* est un  entier naturel alors  que *x*
peut être un nombre, une matrice, un polynôme, un caractère...donc pas forcément
de division, de commutativité. Il faut juste une multiplication associative avec
un élément neutre (un monoïde...).

On pose $`p_0=x`$

La multiplication étant d'arité 2, si on a déjà calculé $`p_1,
	p_2,...,p_{i-1}`$, on peut calculer $`p_i`$ comme produit de deux résultats précédents.

### Méthode naïve

$`p_i=p_{i-1}·p_0`$

Coût en nombre de multiplications ?

## Méthode binaire

On  trouve les  premières traces  de cet  algorithme en  Inde 200  ans avant  JC
d'après KNUTH.

On décompose l'exposant en base 2. On remplace chaque `1` par CX sauf le premier
qu'on passe et chaque `0`
par C. 
Ensuite, la règle est simple : chaque C représente une mise au carré et chaque X
une multiplication par *x*.

Par exemple 22 est `10110` qui donne (CX) C CX CX C et donc dans l'ordre 
$`x^2\ x^4\ x^5\ x^{10}\ x^{11}\ x^{22}`$


Voici par  exemple une traduction de  cet algorithme en utilisant  les notations
de KNUTH :

```console
FONCTION Expo(x: Monoïde, n: Entier) -> Monoïde
# Initialisation
N ⟵ n
Y ⟵ 1
Z ⟵ x
TANT QUE N > 0 FAIRE
	# Division par 2
	# on a x^n = Y.Z^N
	t ⟵ N mod 2
	N ⟵ N // 2
	SI t != 0 
		Y ⟵ Z*Y
	FIN SI
	Z ⟵ Z*Z
FIN TANT QUE
RETOURNER Y
```


|Étape|  t | N | Y | Z  |
|-----|----|---|---|----|
|0    |    |22 | 1 | x  |
| 1   | 0  |11 | 1 |x^2 |
|2    | 1  |5  |x^2|x^4 |
|3    | 1  |2  |x^6|x^8 |
|4    | 0  |1  |x^6|x^16|
|5    | 1  |0  |x^22|x^32|



Coût en nombre de multiplications ?


C'est mieux mais est-ce optimal ? Regarder avec 15 : pouvez-vous faire mieux que
ce que propose l'algorithme binaire ?

## Arbre de KNUTH

Don KNUTH est le dieu de  l'algorithmique. Dans son oeuvre mythique, une section
est consacrée au problème de l'exponentiation (Tome 2, section 4.6.3 pp 461-485)
et KNUTH propose la construction d'un arbre dont voici le début:

```mermaid
graph TD;
1-->2;
2-->3;
2-->4;
3-->5;
3-->6;
4-->8;
5-->7;
5-->10;
6-->9;
6-->12;
8-->16;
7-->14;
10-->11;
10-->13;
10-->15;
10-->20;
9-->18;
12-->24;
16-->17;
16-->32;
14-->19;
14-->21;
14-->28;
11-->22;
13-->23;
13-->26;
15-->25;
15-->30;
20-->40;
18-->27;
18-->36;
24-->48;
17-->33;
17-->34;
32-->64;
```

On construit le *k+1*e niveau à partir  des *k* précédents en suivant le procédé
suivant:

Pour
chaque noeud *n* de la gauche vers la droite, soit $`p_1, p_2,...,p_{k-1}=n`$ ses prédécesseurs le long de la
branche qui remonte à la racine.
S'ils n'existent pas déjà, on crée un lien avec les noeuds: 
$`n+1,\ n+p_1,\ n+p_2,...,n+p_{k-1}=2n`$


KNUTH  a montré  que les  plus  petits nombres  pour  lesquels la  méthode n'est  pas
optimale sont 77 puis 154 puis 233.


Avec quelques raisonnements par récurrence plus ou moins astucieux, on arrive en
fait à montrer que le coût optimal de l'exponentiation est en $`\log n`$ : lisez
"le KNUTH" :)






