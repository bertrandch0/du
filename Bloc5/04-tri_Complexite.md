Tris - Complexité
=================

Dans toute la suite, on se ramènera au cas du tri d'une permutation
quelconque de $\mathfrak{S}_n$. On étudiera les danses hongroises
disponibles en ligne, par exemple:

[![](http://img.youtube.com/vi/kDgvnbUIqT4/0.jpg)](http://www.youtube.com/watch?v=kDgvnbUIqT4
"Quick Sort")

Tri sélectif...
----------------

On parcourt la liste, on cherche le plus grand et on l'échange avec
l'élément le plus à droite et on recommence avec la liste privée du plus
grand élément.

### Version impérative

On commence par chercher l'indice du maximum d'une liste. On part de 0
et on compare à chaque élément de la liste en faisant évoluer l'indice
du maximum si nécessaire.

```python
    def indMaxi(xs):
        indTmp = 0
        n       = len(xs)
        for i in range(n):
            if xs[i] > xs[indTmp]:
                indTmp = i
        return indTmp
```

Ensuite, on copie la liste donnée en argument pour ne pas l'écraser. On
parcourt la liste et on procède aux échanges éventuels entre le maximum
et l'élément de droite.

```python
    def triSelect(xs):
        cs = xs.copy()
        n  = len(cs)
        for i in range(n - 1,0,-1):
            iM = indMaxi(cs[:i + 1])
            if iM != i:
                cs[iM],cs[i] = cs[i],cs[iM]
            # print(cs) : pour suivre l'évolution
        return cs
```

On va utiliser `permutation` de la bibliothèque `numpy.random` qui renvoie une permutation
uniformément choisie par les permutations de $\mathfrak{S}_n$.

```python
    In [5]: ls = list(permutation(range(10)))

    In [6]: ls
    Out[6]: [8, 0, 4, 3, 5, 2, 7, 1, 9, 6]

    In [7]: triSelect(ls)
    [8, 0, 4, 3, 5, 2, 7, 1, 6, 9]
    [6, 0, 4, 3, 5, 2, 7, 1, 8, 9]
    [6, 0, 4, 3, 5, 2, 1, 7, 8, 9]
    [1, 0, 4, 3, 5, 2, 6, 7, 8, 9]
    [1, 0, 4, 3, 2, 5, 6, 7, 8, 9]
    [1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
    [1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
    [1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    Out[7]: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

### Correction

Il s'agit de démontrer la correction des deux fonctions.

Pour la première, l'invariant de boucle est: `indTemp` est l'indice du
maximum des `i + 1` premiers termes de la liste en argument et se
démontre par récurrence.

Pour la deuxième fonction, l'invariant de boucle est la liste des
élements d'indices entre `i` et `n  - 1` est triée.

### Complexité

Nous mesurerons la complexité en nombre de comparaisons.

La complexité de  `ind_maxi(xs)` est en $\Theta(n)$ avec $n$  la longueur de
la liste `xs`.

En effet, il y a une comparaison par itération et $n$ itérations.

Pour `tri_select`, on effectue $n-1$ itérations, et chacune lance `ind_maxi` sur une liste de
longueur $i+1$.

Or
$\displaystyle\sum_{i=1}^{n-1}i+1=\sum_{i=2}^ni=(n-2)\frac{2+n}{2}=\frac{n^2-4}{2}$

donc la complexité est en $\Theta(n^2)$.


```python
    In [10]: ls = list(permutation(100))

    In [11]: %timeit triSelect(ls)
    1000 loops, best of 3: 561 μs per loop

    In [12]: ls = list(permutation(200))

    In [13]: %timeit triSelect(ls)
    100 loops, best of 3: 2.03 ms per loop

    In [14]: ls = list(permutation(400))

    In [15]: %timeit triSelect(ls)
    100 loops, best of 3: 7.89 ms per loop
```

On observe effectivement que le temps est environ multiplié par 4 quand
la taille de la liste double.

Tri par insertion
-----------------

C'est le tri d'un jeu de carte: on insère un élément dans un tableau
trié petit à petit en comparant le nouvel élément à insérer aux éléments
déjà triés.

La version récursive est assez naturelle. On commence par créer une
fonction qui insère un nouvel élément dans la liste:

```python
    def insRec(carte,Main):
        if Main == Vide or carte <= tete(Main):
            return [carte] + Main
        return [tete(Main)] + insRec(carte,queue(Main))
```

Ensuite, pour trier, on insère la tête dans la queue triée...

```python
    def triInsRec(Pioche):
        if Pioche == Vide:
            return Vide
        return insRec(tete(Pioche),triInsRec(queue(Pioche)))
```

Pour la version itérative, on module aussi:

```python
    def insere(y,xs):
        cs = (xs.copy()) + [y] # xs est triée et on insère y par la doite
        n = len(xs)
        i = n
        while cs[i] < cs[i - 1] and i > 0:
            cs[i - 1], cs[i] = cs[i], cs[i - 1]
            i -= 1
            # print(cs)
        return cs

    def triInsere(Pioche):
        Main = Vide
        for carte in Pioche:
            Main = insere(carte, Main)
            #print(Main)
        return Main
```


La justification et la complexité de ces fonctions seront à travailler
en TD, mais la complexité semble encore quadratique:


```python
    In [29]: ls = list(permutation(100))

    In [30]: %timeit triInsere(ls)
    1000 loops, best of 3: 941 μs per loop

    In [31]: ls = list(permutation(200))

    In [32]: %timeit triInsere(ls)
    100 loops, best of 3: 3.67 ms per loop

    In [33]: ls = list(permutation(400))

    In [34]: %timeit triInsere(ls)
    100 loops, best of 3: 13.7 ms per loop
```

Tri fusion
----------

Cette fois, si le tableau a au plus une valeur, il est trié, sinon on
coupe le tableau en deux, on trie ces deux moitiés et on fusionne.



```python
    def triFusion(xs):
        t = len(xs) 
        if t < 2:
            return xs
        return fusion(triFusion(xs[:t//2]), triFusion(xs[t//2:])) 
```
Il reste à définir la fusion:

```python
    def fusion(xs,ys):
        if xs == Vide or ys == Vide:
            return xs + ys
        if tete(xs) < tete(ys):
            return [tete(xs)] + fusion(queue(xs),ys)
        return [tete(ys)] + fusion(xs,queue(ys))
```

La version récursive est naturelle mais pose toujours des problèmes en
Python: vous chercherez donc une version impérative à titre
d'exercice...

L'étude de la terminaison, de la correction et de la complexité devra
également être conduite par vos soins.

Au fait:


```python
    In [46]: ls = list(permutation(100))

    In [47]: %timeit triFusion(ls)
    1000 loops, best of 3: 296 μs per loop

    In [48]: ls = list(permutation(200))

    In [49]: %timeit triFusion(ls)
    1000 loops, best of 3: 642 μs per loop

    In [50]: ls = list(permutation(1000))

    In [51]: %timeit triFusion(ls)
    100 loops, best of 3: 3.88 ms per loop
```


Tri rapide
----------

### Le tri et ses comlexités

Observez et commentez:


```python
    def partition(pivot,seq):
        p,m,g = [],[],[]
        for item in seq:
            (p if item < pivot else (g if item > pivot  else m)).append(item)
        return p,m,g

    def triRapide(xs):
        if estVide(xs):
            return Vide
        else:
            pivot = tete(xs)
            p,m,g = partition(pivot, xs)
            return (triRapide(p)) + m + (triRapide(g))
```


Pour la complexité en moyenne, démontrez que
$K(n)=n-1+\frac{1}{n}\sum_{i=0}^{n-1}(K(i)+K(n-1-i))$

et en déduire que $\frac{K(n)}{n+1}=2\sum_{i=2}^n\frac{i-1}{i(i+1)}$

Que se passe-t-il dans le pire des cas?

Dans quel cas est-on sûr que le tri en $n\log n$?

### La médiane en temps linéaire

Il serait très pratique d'avoir une médiane en temps linéaire: nous
aurions alors un tri rapide en $n\log n$ dans le pire des cas.

La médiane d'un tableau de $n$ éléments est l'élément de rang
$\lfloor \frac{n+1}{2}\rfloor$ comme vous l'avez appris au collège.

L'idée est bien sûr d'éviter de trier le tableau pour trouver la médiane
:-)

On cherche donc le tableau de rang $r_m=\lfloor \frac{n+1}{2}\rfloor$:
on va utiliser la fonction `partition` en la modifiant légèrement pour
qu'elle renvoie l'indice du pivot après partition. Si cet indice est
égal à $r_m$ on a gagné, s'il est plus petit on cherche la médiane sur
la partie droite, sinon sur la partie gauche.

Y a plus qu'à...




RECHERCHES
----------


### Complexité


1.  Écrire une fonction factorielle qui prend en argument un entier
    naturel $n$ et renvoie $n!$ (on n'accepetra pas bien sûr de réponse
    utilisant la propre fonction factorielle du module math de Python ou
    Scilab).

2.  Écrire une fonction `seuil` qui prend en argument un entier $M$ et
    renvoie le plus petit entier naturel $n$ tel que $n!>M$.

3.  Écrire une fonction booléenne nommée `est_divisible`, qui pend en
    argument un entier naturel $n$ et renvoie `True` si $n!$ est
    divisible par $n+1$ et `False` sinon.

4.  On considère la fonction suivante nommée `mystere`:

```python
        def mystere(n):
            s = 0
            for k in range(1,n+1):
                s = s + factorielle(k)
            return s
```


1.  Quelle valeur renvoie `mystere(4)`?

2.  Déterminer le nombre de multiplications qu'effectue
        `mystere(n)`.

3.  Proposer une amélioration du script de la fonction `mystere`
        afin d'obtenir une complexité linéaire.


### Complexité



1.  Donnez la décomposition en binaire (base 2) de l'entier 21.

    On considère la fonction `mystere` suivante:

```python
        def mystere(n, b) :
            """Données : n > 0 un entier et b > 0 un entier
               Résultat : ......."""
            t = [] # tableau vide
            while n > 0 :
                c = n % b
                t.append(c)
                n = n // b
            return t
```

On rappelle que la méthode `append` rajoute un élément en fin de
    liste. Si l'on choisit par exemple `t = [4,5,6]`, alors, après avoir
    exécuté `t.append(12)`, la liste a pour valeur `[4,5,6,12]`.

Pour tout entier naturel non nul *k*, on note $c_k$, $t_k$ et $n_k$ les valeurs
    prises par les variables `c`, `t` et `n` à la sortie de la $k$-ème
    itération de la boucle while.

2.  Quelle est la valeur renvoyée lorsqu'on exécute `mystere(256, 10)`?

    On recopiera et complètera le tableau suivant en rajoutant les
    colonnes nécessaires pour tracer entièrement l'exécution:

      | $k$  |  1 |  1 |  $\cdots$|
      |------- |---| --- |----------|
       |$c_k$| | |$\cdots$|
       |$t_k$| | |$\cdots$|
       |$n_k$| | |         $\cdots$|

3.  Soit $n>0$ un entier. On exécute `mystere(n, 10)`. On pose $n_0=n$.

    1.  Justifier la terminaison de la boucle `while`.

    2.  On ote $p$ le nombre d'itérations lors de l'exécution de `mystere(n, 10)`.
        Justifiez que pour tout $k\in[\![0,  p]\!]$, on a $n_k \leqslant  \frac{n}{10^k}$.
        Déduisez-en une majoration de *p* en fonction de *n*.

4.  En  vous aidant  du script  de la fonction  `mystere`, écrivez  une fonction
    `somme_chiffres` qui
    prend en argument un entier naturel et renvoie la somme de ses
    chiffres. Par exemple `somme_chiffres(256)`, devra renvoyer 13.

5.  Écrivez une version récursive de la fonction `somme_chiffres` que vous nommerez `somme_rec`.


### Point fixe


On dispose d'un tableau de N entiers (relatifs) distincts rangés dans
l'ordre croissant.

Déterminez un algorithme qui renvoie l'éventuel indice `i` tel
que `a[i] = i`

### Recherche en dent de scie

Un tableau est constitué d'une suite croissante d'entiers directement
suivie d'une suite décroissante d'entiers. Tous les entiers sont
supposés distincts. Déterminez une fonction qui détermine si un entier
appartient ou non à un tableau en dent de scie. Votre programme doit
effectuer $\sim  3\log N$ comparaisons dans le pire des cas.




### Écart mumuxam




Étant donné un tableau de N réels, déterminez un algo en temps linéaire
qui détermine la valeur maximum de `a[j]-a[i]}` pour
$j \geqslant i$.




### Multiplication du paysan russe


Voici ce qu'apprenaient les petits soviétiques pour multiplier deux
entiers. Une variante de cet algorithme a été retrouvée sur le papyrus
de Rhind datant de 1650 avant JC, le scribe Ahmes affirmant que cet
algorithme était à l'époque vieux de 350 ans. Il a survécu en Europe
occidentale jusqu'aux travaux de Fibonacci.

![image](./IMG/rhind.jpg)



```console
FONCTION MULRUSSE( x entier ,y: entier, acc:entier) → entier
   SI x==0
      RETOURNΕ acc
   SINON
      SI x est pair
         RETOURNE MULRUSSE(x/2,y*2,acc)
      SINON
         RETOURNE MULRUSSE((x-1)/2,y*2,acc+y)
      FINSI
   FINSI
```


Que vaut `acc` au départ?

Écrivez une version récursive de cet algorithme en évitant l'alternative
des lignes 5 à 9.

Écrivez une version impérative de cet algorithme.

Prouvez la correction de cet algorithme.

Étudiez sa complexité.


En python, `x >> 1` décale l'entier *x* d'un bit vers la doite et `x << 1` décale *x* d'un
bit vers la gauche en complétant par un zéro à droite, `x & y` renvoie l'entier
obtenu en faisant la conjonction logique bit à bit des représentations
binaires de *x* et *y* et `~x` renvoie le complément à 1 de *x*.

Ré-écrivez la multiplication russe en utilisant que ces opérations bit à
bit (pas de division ni de multiplication).




### Tri sportif

Que pensez-vous de la complexité de cet algorithme de tri?

(source: <http://unclecode.blogspot.tw/2012/02/stupid-sort_2390.html>):

```cpp
void StupidSort()
{
    int i = 0;
    while(i < (size - 1))
    {
           if(data[i] > data[i+1])
          {
                int tmp   = data[i];
                data[i]   = data[i+1];
                data[i+1] = tmp;
                i = 0;
          }
          else
         {
               i++;
         }
     }
}
```


### Mélangge de cartes - le mélange de ASF Software

Savoir bien trier c'est aussi savoir bien mélanger.

Afin de convaincre les utilsateurs que leur algorithme de mélange était
juste, la société ASF Software, qui produit les logiciels utilisés par
de nombreux sites de jeu, avait publié cet algorithme:

```pascal
procedure TDeck.Shuffle;
var
    ctr: Byte;
    tmp: Byte;

    random_number: Byte;
begin
    { Fill the deck with unique cards }
    for ctr := 1 to 52 do
        Card[ctr] := ctr;

    { Generate a new seed based on the system clock }
    randomize;

    { Randomly rearrange each card }
    for ctr := 1 to 52 do begin
        random_number := random(51)+1;
        tmp := card[random_number];
        card[random_number] := card[ctr];
        card[ctr] := tmp;
    end;

    CurrentCard := 1;
    JustShuffled := True;
end;
```

1.  Considérez un jeu de 3 cartes. Dressez l'arbre de tous les mélanges
    possibles en suivant cet algorithme. Que remarquez-vous?

2.  Proposez un algorithme qui corrige ce problème. Dressez l'arbre
    correspondant pour un jeu de trois cartes.

3.  Traduisez la fonction proposée en Python puis votre version corrigée
    en Python.



### Yet Another Sort of Sort


```
POUR i de 1 à n Faire
   POUR j de n à i + 1 par pas de -1 Faire
      SI t[j] < t[j-1]
         Échange t[j] et t[j-1]
      FIΝ SI
   FIΝ POUR
FIN POUR
```


Qu'est-ce que c'est? Qu'est-ce que ça fait? Comment ça marche?
Complexité? En python? Donnez un nom à ce tri.




### Comparaisons de tris et tracés de graphiques


Tracez sur un même graphique les temps d'exécution des tris étudiés en
fonction de la longueur d'une liste mélangée.

Sur un autre graphique, comparez ces tris avec des listes ordonnées dans
le sens croissant, puis avec des listes ordonnées dans le sens
décroissant.

Pour cela, voici quelques rappels sur Matplotlib.

On lance ipython avec l'option `-pylab` ou bien, si vous ne travaillez pas dans
un confortable terminal Unix, vous pouvez toujours importer la chose:

```python
    In [29]: import matplotlib.pyplot as plt

    In [30]: p1 = plt.plot([1,2,3,4,5],[1,2,5,8,3], marker = 'o',label = "Idiot")

    In [31]: p2 = plt.plot([1,2,3,4,5],[3,8,2,6,-1], marker = 'v',label = "Stupide")

    In [32]: plt.legend()
    Out[32]: <matplotlib.legend.Legend at 0x7fbc2c6b76d8>

    In [33]: plt.title("Essai idiot")
    Out[33]: <matplotlib.text.Text at 0x7fbc2babc668>

    In [34]: plt.xlabel("Le temps")
    Out[34]: <matplotlib.text.Text at 0x7fbc2c620898>

    In [35]: plt.ylabel("C'est de l'amour")
    Out[35]: <matplotlib.text.Text at 0x7fbc2baaa2b0>

    In [36]: plt.show()

    In [37]: plt.savefig("zig.pdf")

    In [38]: plt.clf()
```

![image](./IMG/zig.png)

Comment mesurer le temps? On utilise la fonction `perf_counter` de la bibliothèque `time`:

    from time import perfCounter

    def temps(tri,p):
        debut = perfCounter()
        tri(p)
        return perfCounter() - debut

On devrait obtenir:

![image](./IMG/benchmark.png)

et en regardant les trois plus rapides:

![image](./IMG/benchmark2.png)

mais si les listes sont déjà rangées:

![image](./IMG/benchmark3.png)

Comme le nombre d'appels récursifs va augmenter, on va augmenter aussi
le nombre d'appels récursifs autorisé:

    from sys import setrecursionlimit
    setrecursionlimit(100000)



### Politique américaine


![image](./IMG/smith.jpg)

Le Sénat américain est composé de deux sénateurs par état (il y a 50
états...). Vous allez récupérer les statistiques sur 46 votes du
109e congrès (2006). Un 1 signifie un Yea, un $-1$ un Nay et un 0 une
abstention. Il manque les votes de Jon CORZINE, sénateur
du New-Jersey qui a été victime d'un accident de voiture cette année-là
(cela fait une exception à gérer :-/ ).

On utilisera les fonctions `open, read, split` pour récupérer les données.

1.  Créez un dictionnaire `vs` de type `{Nom : liste des votes}` et un autre, `os`, de type `{Nom : [Parti, État]}`.

    Par exemple:

        In [18]: os['Obama']
        Out[18]: ['D', 'IL']

        In [20]: vs['Obama']
        Out[20]: [1, -1, 1, 1, 1, -1, -1,...]

    On pourra utiliser la fonction `int`et des définitions par compréhension.

2.  Comment utiliser le produit scalaire pour mesurer l'accord ou le
    désaccord entre deux sénateurs?

3.  Écrivez une fonction `compare(sen1, sen2)` qui mesure le degré d'accord de deux sénateurs.
    Par exemple:

        In [21]: compare('Obama', 'McCain')
        Out[21]: 16

4.  Écrivez une fonction `senat_parti(parti)` qui renvoie l'ensemble des noms des sénateurs
    d'un parti donné. Créez aussi une fonction qui renvoie le nom des
    sénateurs par état, le nom des états par parti.

5.  Écrivez une fonction `compare_sen_etat(etat)` qui mesure la cohérence des votes des deux
    sénateurs d'un état donné.

        In [23]: compareEtat('WA')
        Out[23]: (39, 'WA', ('Cantwell', 'Murray'))  

    Classez les états dans l'ordre croissant des cohérences.

6.  Créez une fonction `plus_eloigne(sen, ens)` qui renvoie le nom et la mesure de l'écart du
    sénateur d'un ensemble donné le plus en désaccord avec un sénateur
    donné.

        In [33]: plusEloigne('Obama',senatParti('D'))
        Out[33]: (22, 'Nelson2')

        In [34]: plusEloigne('Obama',senatParti('R'))
        Out[34]: (7, 'Sununu')

    Faites de même avec le plus en accord.

7.  Quel est le parti le plus cohérent? Comment le déterminer à l'aide
    d'une fonction calculant une moyenne de cohérence.

    Par souci d'efficacité, on pourra se souvenir que le produit
    scalaire est distributif sur l'addition des vecteurs.

8.  Écrivez une fonction `moinsDaccord(ens)` qui renvoie le couple de sénateurs les moins
    d'accord d'un ensemble donné:

        In [50]: moinsDaccord(senateurs)
        Out[50]: (('Feingold', 'Inhofe'), -3)  

    Combien de calculs et de comparaisons sont effectués? Pensez-vous
    pouvoir être plus efficace?

9.  Qui est le sénateur le plus Républicain? L'État le plus Démocrate?
    Classez les sénateurs/États selon ces critères.

10. Ouvrez le fichier [`ONU_vote.txt`](ONU_vote.txt) et étudiez-le d'une manière similaire à ce qui
    vient d'être fait.


### MURD3RS


Les américains ont un goût prononcé pour les tueurs en série. Le profil
psychologique de certains de ces héros maléfiques correspond à une
personne logique, froide, méthodique, dangereuse, qui connait bien son
métier, qui travaille suffisamment lentement pour bien faire son boulot
et suffisamment rapidement pour être efficace. Certains sont des
sociopathes, d'autres sont très à l'aise en société : on retrouve
exactement les qualités recherchées chez un programmeur...Bref, si on
apprenait plus de Caml à l'école, les tueurs en série disparaîtraient.
Un policier canadien, Kim ROSSMO, s'est lui aussi
passionné pour les mathématiques et la programmation et devint en 1995
le premier policier canadien à obtenir un doctorat en crimonologie
(<https://en.wikipedia.org/wiki/Kim_Rossmo>). Il a publié un article
(<http://www.popcenter.org/library/crimeprevention/volume_04/10-Rossmo.pdf>)
sur l'analyse géographique des crimes et proposa une formule pour
localiser le lieu d'habitation d'un tueur en série, ou plutôt pour
calculer le lieu le plus probable :

$p_{i,j} = k \sum_{n=1}^{(\mathrm{nb\; total\;de\;crimes})} \left [ \underbrace{ \frac{\phi_{ij}}{ (|X_i-x_n| + |Y_j-y_n|)^f } }_{ 1^{\mathrm{er}}\mathrm{\;terme} } + \underbrace{ \frac{(1-\phi_{ij})(B^{g-f})}{ (2B - \mid X_i - x_n \mid - \mid Y_j-y_n \mid)^g } }_{ 2^{\mathrm{nd}}\mathrm{\;terme} } \right ],$

avec

$\phi_{ij} = \begin{cases} 1, & \mathrm{\quad si\;} ( \mid X_i - x_n \mid + \mid Y_j - y_n \mid ) > B \quad \\ 0, & \mathrm{\quad sinon} \end{cases}$
Bon, plus synthétiquement, ça peut s'écrire :

$P(x)=\sum_{\mathrm{lieu       du      crime       c}}\frac{φ}{d(x,c)^f}      +
\frac{(1-φ)B^{g-f}}{(2B-d(x,c))^g }$ 

....euh...qui est qui là-dedans ? On discrétise un plan, de ville de
préférence, à l'aide d'une grille. On calcule alors la probabilité que
le tueur habite dans la case *x*.

Comme on travaille dans une ville, la distance $d$ sera la distance du
taxi de Manhattan :

![image](./IMG/manhattan.png)

ou si vous préférez:

![image](./IMG/pacman.png)

B est le rayon de la Buffer zone  ou zone tampon : c'est un cercle 
(au sens des taxis) autour du lieu du crime. La fonction $φ$ est la
fonction caractéristique de cette zone tampon: elle vaut 1 si la cellule
est dans la zone et 0 sinon. $f$ et $g$ sont des paramètres arbitraires
qui permettent de s'adapter aux données des crimes précédents. On
cherche à présent à dresser une carte des probabilités. On va utiliser
la fonction `pcolormesh` de `matplotlib.pyplot`. Vous pouvez regarder l'aide. Ce qui va motiver notre
construction, c'est la nature des arguments de cette fonction :

```python
    def dessineRossmo( lieux, vrai, B, f, g, tailleX, tailleY) :
       tX = np.arange(tailleX)
       tY = np.arange(tailleY)
       X, Y = np.meshgrid(tX, tY)
       Z = np.vectorize( probaRossmo(lieux, B, f, g) )(X, Y)
       g = cg(lieux)
       plt.pcolormesh( X, Y, Z, cmap = cm.jet)# , shading=’gouraud’)
       plt.plot(vrai[0], vrai[1], ’ks’, mew = 7)
       plt.plot(g[0],g[1], ’ko’, mew = 7)
       plt.colorbar()
       plt.show()
```

Ici, `lieux` est l'ensemble des lieux des crimes, `vrai` est le
véritable domicile du tueur, `taille` et `taille` sont les dimensions de
la grille.

La fonction `cg` calcule les coordonnées du centre de gravité des lieux
des crimes pour comparer avec le candidat de la méthode Rossmo.

`colorbar` permet d'avoir une échelle de couleur pour lire la carte. Il
va falloir bien comprendre la construction de Z... À vous de mener
l'enquête informatique.
ROSSMO fournit des données pour deux tueurs en série
célèbres:

-   le vampire de Sacramento
    (<https://en.wikipedia.org/wiki/Richard_Chase>)

        vampire = {(3, 17), (15, 3), (19, 27), (21, 22), (25, 18)}
        vraiVampire = ([19],[17])  

-   L'étrangleur de Boston
    (<https://en.wikipedia.org/wiki/Albert_DeSalvo>)

        boston = [[10, 48], [13, 8], [15, 11], [17, 8], [18, 7],
                  [18, 9], [19, 4], [19, 8], [20, 9], [20, 10], [20, 11],
                  [29, 23], [33, 28]]  
        vraiBoston = ([19],[18])

Vous obtiendrez des cartes qui ressemblent à ça :

![image](./IMG/gradientCrime.png)
