# Complexité

## Approche expérimentale

### Le problème

On dispose d'une liste de N nombres.  Déterminez le nombre de 
  triplets dont la somme est nulle
  
```python
def trois_sommes(xs):
    N = len(xs)
    cpt = 0
    for i in range(N):
        for j in range(i + 1, N):
            for k in range(j + 1, N):
                if xs[i] + xs[j] + xs[k] == 0:
                    cpt += 1
    return cpt
```




On mesure le temps d'exécution:

```python
In [1]: %timeit trois_sommes([randint(-10000,10000) for k in range(100)])
10 loops, best of 3: 27.5 ms per loop

In [2]: %timeit trois_sommes([randint(-10000,10000) for k in range(200)])
1 loops, best of 3: 216 ms per loop

In [3]: %timeit trois_sommes([randint(-10000,10000) for k in range(400)])
1 loops, best of 3: 1.82 s per loop
```


Avec des listes par compréhension?

```python
def trois_sommes_comp(xs):
    n = len(xs)
    return len([(i,j,k) for i in range(n) for j in range(i+1, n) for k in range(j+1,n) if xs[i] + xs[j] + xs[k] == 0 ])  
```

Pas de différence notable :(


```python
In [24]: %timeit trois_sommes_comp([randint(-10000,10000) for k in range(100)])
10 loops, best of 3: 28.1 ms per loop

In [25]: %timeit trois_sommes_comp([randint(-10000,10000) for k in range(200)])
1 loops, best of 3: 222 ms per loop

In [26]: %timeit trois_sommes_comp([randint(-10000,10000) for k in range(400)])
1 loops, best of 3: 1.85 s per loop  
```


Cherchons à mieux comprendre cette évolution:


```python
from time import perf_counter
from math import log2

def temps(xs):
    debut = perf_counter()        # on déclenche le chrono
    trois_sommes(xs)              # on lance le calcul
    return perf_counter() - debut # on arrête le chrono quand c'est fini

# on fabrique une liste contenant les temps de calcul pour des longueurs de 100, 200, 400 et 800
t = [temps(range(100 * 2**k)) for k in range(4)]

# on forme la liste des ratios 
ratio = [t[k + 1] / t[k] for k in range(3)]

# on applique la fonction log2 aux éléments de la liste des ratios
logratio = [log2(r) for r in ratio]

```

Alors:

```python
In [4]: ratio
Out[4]: [7.523860206447286, 9.118789882406599, 8.5098312160934]

In [5]: logratio
Out[5]: [2.940628541715559, 3.133284732580891, 3.128693841844642]  
```

On voit un motif se dessiner...

Il semble  y avoir  proportionnalité entre  la taille  de la  liste et  le temps
d'exécution. Soit `a` le rapport.

```python
In [6]: temps(range(400))
Out[6]: 4.005484320001415  
```



$4,00 = a\times 400^3 $ donc $a\approx 6,25\times 10^{-8}$

Donc   pour   $N=1000$,    on   devrait   avoir   un    temps   de   $6,25\times
10^{-8}\times10^9=62,5$


```python
In [7]: temps(range(1000))
Out[7]: 68.54615448799996  
```

### En C

Avec ce petit programme:

```c
#include <stdio.h>
#include <time.h>


typedef int Liste[13000];

int trois_sommes(int N)
{
  int cpt = 0;
  Liste liste;

  for ( int k = 0; k < N; k++)
    {
      liste[k] = k - (N / 2);
    }
  
  for (int i = 0; i < N; i++)
    {
      int xi = liste[i];
      for (int j = i + 1; j < N; j++)
	{
	  int sij = xi + liste[j];
	  for (int k = j + 1; k < N; k++)
	    {
	      if (sij + liste[k] == 0) {cpt++;}
	    }
	}
     }
printf("%d\n",cpt);
  return cpt;
}


void chrono(int N)
{
  clock_t temps_initial, temps_final;
  float temps_cpu;

  temps_initial = clock();
  trois_sommes(N);
  temps_final   = clock();
  temps_cpu = ((double) temps_final - temps_initial) / CLOCKS_PER_SEC;
  printf("Temps en sec pour %d : %f\n",N, temps_cpu);

}
  


int main(void)
{
  chrono(100);
  chrono(200);
  chrono(400);
  chrono(800);
  chrono(1600);
  chrono(3200);
  chrono(6400);
  // chrono(12800);

  return 1;
}
```

on obtient:

```bash
$ gcc  -std=c99 -Wall -Wextra  -Werror -pedantic -O4 -o somm3 Trois_Sommes.c
$ ./somm3 
Temps en sec pour 100 : 0.00000
Temps en sec pour 200 : 0.000000
Temps en sec pour 400 : 0.020000
Temps en sec pour 800 : 0.090000
Temps en sec pour 1600 : 0.720000
Temps en sec pour 3200 : 5.760000
Temps en sec pour 6400 : 45.619999
Temps en sec pour 12800 : 360.839996
```



On a la même évolution en $N^3$ avec  un rapport de 8 entre chaque doublement de
taille  mais la constante est bien meilleure:

$45,61 = a\times 6400^3$ d'où $a\approx 1,74\times10^{-10}$

$360,84 = a\times 12800^3$ d'où $a\approx 1.72\times 10^{-10}$


Le compilateur  C est  en fait intelligent  et évite en  fait de  recalculer des
valeurs déjà connues.

Aidons le pauvre `python` à faire de même:


```python
def trois_sommes(xs):
    N = len(xs)
    cpt = 0
    for i in range(N):
        xi = xs[i]
        for j in range(i + 1, N):
            sij = xi + xs[j]
            for k in range(j + 1, N):
                if sij + xs[k] == 0:
                    cpt += 1
    return cpt  
```

On gagne un peu de temps:

```python
In [28]:  xs = list(range(-50,51))
In [29]: %timeit trois_sommes(xs)
100 loops, best of 3: 12.9 ms per loop

In [30]:  xs = list(range(-100,101))
In [31]: %timeit trois_sommes(xs)
10 loops, best of 3: 94.4 ms per loop

In [32]:  xs = list(range(-200,201))
In [33]: %timeit trois_sommes(xs)
1 loops, best of 3: 851 ms per loop

In [34]:  xs = list(range(-400,401))
In [35]: %timeit trois_sommes(xs)
1 loops, best of 3: 7.04 s per loop  
```

mais la progression semble toujours la même.

### Loi de Brooks

*Adding manpower to a late software project  makes it later a result of the
fact that  the expected  advantage from  splitting work  among N  programmers is
$O(N)$, but the complexity and  communications cost associated with coordinating
and then merging their work is $O(N^2)$*

## Grand O, Omega, Theta

>Soit *f* et *g* deux fonctions  de $\bbn$ dans $\bbr$. On dit que $f$
>est un  **grand O** de *g* et on note $f=O(g)$ ou $f(n)=O(g(n))$ si,
>et seulement si, il existe une constante strictement positive C telle que 
> $|f(n)|\leqslant C|g(n)|$ pour tout $n\in \bbn$.



>Soit $f$ et $g$ deux fonctions  de $\bbn$ dans $\bbr$. On dit que $f$
>est un  **grand Oméga** de $g$ et on note $f=\Omega(g)$ ou $f(n)=O(g(n))$ si,
>et seulement si, il existe une constante strictement positive C telle que 
> $|f(n)|\geqslant C|g(n)|$ pour tout $n\in \bbn$.

>**Grand Théta**
> $f=\Theta(g)\Longleftrightarrow f=O(g)\ \ \wedge \ \ f=\Omega(g)$



|coût/n| 100 |1000 |$10^6$ |$10^9$|
|------|-----|-----|----------|--------|
|$\log_2(n)$| ≈ 7| ≈ 10| ≈ 20| ≈ 30|
|$n\log_2(n)$| ≈ 665| ≈ 10 000| ≈ $2\times10^7$| ≈$3\times10^{10}$|
|$n^2$|≈ $10^{4}$| ≈$10^{6}$| ≈$10^{12}$| ≈$10^{18}$|
|$n^3$|≈ $10^{6}$| ≈$10^{9}$| ≈$10^{18}$| ≈$10^{27}$|
|$2^n$|≈ $10^{30}$| ≈$10^{300}$| ≈$10^{10^5}$| ≈$10^{10^8}$|


Gardez en tête que l'âge de l'Univers est environ de $10^{18}$ secondes...


## Calcul effectif


```python
def trois_sommes(xs):
    N = len(xs)
    cpt = 0
    for i in range(N):
        xi = xs[i]
        for j in range(i + 1, N):
            sij = xi + xs[j]
            for k in range(j + 1, N):
                if sij + xs[k] == 0:
                    cpt += 1
    return cpt 
```

|OPÉRATION | FRÉQUENCE|
|----------|----------|
|Déclaration de la fonction et du paramètre (l. 1)| 2|
|Déclaration de N, cpt et i (l. 2, 3 et 4) |3|
|Affectation de N, cpt et i (l. 2, 3 et 4)| 3|
|Déclaration de xi (l. 5) | N|
|Affectation de xi (l. 5) | N|
|Accès à xs[i] (l. 5) | N|
|Déclaration de j (l.6) | N|
|Calcul de l'incrément de i (l. 6) | N|
|Affectation de j (l.6) | N|


|OPÉRATION | FRÉQUENCE|
|----------|-----------|
|Déclaration de sij (l. 7) | $S_1$|
|Affectation de sij (l. 7) | $S_1$|
|Accès à xs[j] (l.7) | $S_1$|
|Somme (l.7) | $S_1$|
|Déclaration de k (l.8) | $S_1$|
|Incrément de j (l. 8) | $S_1$|
|Affectation de k (l.8) | $S_1$|
|Accès à x[k] (l.9) | $S_2$|
|Calcul de la somme (l.9) | $S_2$|
|Comparaison à 0 (l.9) | $S_2$|
|Incrément de cpt (l.9) | entre 0 et $S_2$|
|Affectation de la valeur de retour (l.11) |1|





**Que valent $S_1$ et $S_2$?**

$S_1=\displaystyle\sum_{i=0}^{N-1}N-(i+1)=\sum_{i'=0}^{N-1}i'=\frac{N(N-1)}{2}        \qquad
(\text{avec } i'=N-(i+1))$


$
\begin{eqnarray*}
  S_2&=&\sum_{i=0}^{N-1}\sum_{j=i+1}^{N-1}N-(j+1)\\
     &=&\sum_{i=0}^{N-1}\sum_{j'=0}^{N-(i+2)}j' \qquad (j'=N-(j+1))\\
     &=&\sum_{i=0}^{N-2}\frac{(N-(i+2))(N-(i+1))}{2} \\
     &=&\sum_{i'=1}^{N-2}\frac{i'(i'+1)}{2}\qquad (i'=N-(i+2))\\
     &=&\frac{1}{2}\left(\sum_{i=1}^{N-2}i'^2+\sum_{i=1}^{N-2}i'\right)\\
     &=&\frac{1}{2}\left(\frac{(N-2)(2N-3)(N-1)}{6}+\frac{(N-2)(N-1)}{2}\right)\\
     &=&\frac{N(N-1)(N-2)}{6}
\end{eqnarray*}
$


Notons *a* le temps constant d'affectation, *d* le temps constant de
déclaration, *x* le temps constant d'accès à une cellule, *s* le temps constant
d'une somme, *c* le temps constant d'une comparaison.

Le temps d'exécution vérifie:

$
\begin{multline}
  \tau(N) \leqslant (2d+3d+3a+a) + (d+a+x+d+s+a)N+(d+a+x+s+d+s+a)S_1 +(x+s+c+s)S_2
\end{multline}
$

Or $S_1\sim N^2$ et $S_2\sim N^3$ quand N est *grand*.


Finalement... $\tau(N)=O(N^3)$


## Algorithme de Karatsouba

* Addition de deux entiers de $n$ chiffres: $O(n)$

* Multiplier un entier de $n$ chiffres par un entier de 1 chiffre: $O(n)$
* Algorithme de l'école  primaire pour multiplier deux  nombres de
$n$ chiffres : $n$ multiplications d'un nombre de $n$ chiffres par un
nombre de 1 chiffre puis une addition des $n$ nombres obtenus: $O(n^2)$



>**Idée**  $O(n^2)$: multiplication  de nombres  deux fois  plus petits  $⟶$
>quatre fois plus rapide ?


$xy=(10^mx_1+x_2)(10^my_1+y_2)=10^{2m}x_1y_1+10^m(x_2y_1+x_1y_2)+x_2y_2$




```
Fonction MUL(x: entier, y:entier) -> entier
Si n==1 Alors
   Retourner x.y
Sinon
   m  <- ENT(n/2)
   x1 <- ENT(x/10^m)
   x2 <- x mod 10^m
   y1 <- ENT(y/10^m)
   y2 <- y mod 10^m
   a  <- MUL(x1, y1, m)
   b  <- MUL(x2, y1, m)
   c  <- MUL(x1, y2, m)
   d  <- MUL(x2, y2, m)
   Retourner a.10^2m + (b +c).10^m + d
FinSi
```




Les divisions et  les multiplications par des  puissances de 10 ne  sont que des
décalages effectués en temps constant.

L'addition finale est en $\lambda n$ donc le temps d'exécution est défini par:

$T(n)=4T(\lfloor n/2\rfloor) +\lambda n \qquad T(1)=1$



 $n=2^k  \qquad T(n)=T(2^k)=x_k$




$x_k=4x_{k-1}+\lambda 2^k \qquad x_0=1$

$
 \begin{array}{rcl}
x_k&=&4(4x_{k-2}+\lambda' 2^{k-1})+\lambda 2^k\\
  &=&  4^kx_0+\sum_{i=1}^k\Lambda_k  2^k\\
&=&4^k+k\Lambda_k 2^k \\
&=&n^2 + \Lambda_k n \log n \\
&=& Θ(n^2)    
  \end{array}
$


![kolmogorov](./IMG/kolmogorov.jpg)


```
:/
```



![karatsouba](./IMG/karatsouba.jpg)


$bc + ad = ac+bd -(a-b)(c-d)$

```
:)
```

**En quoi cela simplifie le problème? Quelle est alors la nouvelle complexité?**


## Diviser pour régner: ze Master Theorem


![Sun Zi (544–496 av. J.-C.)](./IMG/suntzu.jpg)


*Le commandement  du grand nombre  est le même pour  le petit nombre,  ce n'est qu'une question de division en groupes.*

*孙子兵法 VIe siècle avant JC*




On considère un problème  de taille n qu'on découpe en  a sous-problèmes de taille
n/b avec a et b des entiers. Le coût de l'algorithme est alors:

$
\begin{cases}
  T(1) = 1\\
  T(n) = a×T(n/b) + \text{Reconstruction(n)}
\end{cases}
$


En général la reconstruction est de l'ordre de $c×n^α$.



Par exemple, pour l'algorithme de КАРАЦУБА nous avions  
$T(n)=3T(\lfloor n/2\rfloor)  +\Theta(n)$
$a=3$, $b=2$, $α=1$ et *c* quelconque avec *n* pair.




$ \begin{array}{rcl}
T(n)&=&aT(n/b)+cn^α\\
&=&a^2T(n/b^2)+ac(n/b)^α+cn^α\\
&=&\cdots\\
&=&a^kT(n/b^k)+\sum_{i=0}^{k-1}a^ic(n/b^i)^α 
\end{array}
$


$n=b^{k_n}$ 


$T(n)=a^{k_n}+cn^α\sum_{i=0}^{k_n-1}(a/b^α)^i$


$T(n)=a^{k_n}+cn^α\sum_{i=0}^{k_n-1}(a/b^α)^i$


*  $a> b^α$  alors la  somme géométrique  est équivalente  au premier  terme
  négligé    à     une    constante    multiplicative    et     additive    près
  ($\frac{1}{a/b^α-1}\left(\frac{a}{b^α}\right)^{k_n} -\frac{1}{a/b^α-1} $ ) à savoir $(a/b^α)^{k_n}$ et $T(n)=O(a^{k_n})$ soit
  $T(n)=O(n^{\log_b(a)})$
* $a=b^α$ alors $T(n)=O(n^α\log_b(n))$
* $a <  b^α$ alors la série géométrique  est convergente: $S(n)\simeq
  n^{\log_b(a)}+cn^α/(1-a/b^α)$ or $a<b^α$ donc $\log_b(a)<α$. Finalement $T(n)=O(n^α)$
  
  
  


Pour revenir à l'algorithme de 
КАРАЦУБА
, $a=3$ et $b^α=2$ donc $T(n)=\Theta(n^{\log_23})$.
