# Programmation dynamique

C'est un  terme historique mais  faussement adapté à la  situation algorithmique
qu'il désigne.

## Première approche : retour sur Fibonnacci


Nous avons  parlé à la  section précédente  de la suite  de Fibonacci et  de ses
désastreux appels récursifs:

```mermaid
graph TD;
6-->5;
6-->4.1;
5-->4.2;
5-->3.1;
4.1-->3.2;
4.1-->2.1;
4.2-->3.3;
4.2-->2.2;
3.1-->2.3;
3.1-->1.1;
3.2-->2.4;
3.2-->1.2;
2.1-->1.3;
2.1-->0.1;
3.3-->2.5;
3.3-->1.4;
2.2-->1.5;
2.2-->0.2;
2.3-->1.6;
2.3-->0.3;
2.4-->1.7;
2.4-->0.4;
2.5-->1.8;
2.5-->0.5;
```

### Top Down

Nous  avons vu  à  la section  précédente  comment remédier  à  ce problème  par
mémoïsation qui  est un algorithme *Top-Down*:  on commence à *n*  et on descend
vers le cas terminal, *0*. La mise en oeuvre est naturellement récursive.


```python
def memoise(f: Callable[[A], B]) -> Callable[[A], B]:
    cache: Dict[A, B] = {}
    def f_avec_cache(n):
        if n not in cache:
            cache[n] = f(n)
        return cache[n]
    return f_avec_cache

@memoise
def fib(n: int) -> int:
    return 0 if n==0 else 1 if n==1 else fib(n-2) + fib(n-1)
```


### Bottom Up

On commence  par les sous-problèmes de  taille supérieure et on  remonte vers le
problème  de   taille  *n*.  La  version   est  plus  aisément  mise   en  forme
itérativenent:

```python
def FibBU(n:int) -> int:
	cache: List[int] = [0]*(n + 1)
	cache[1] = 1
	for i in range(2, n + 1):
		cache[i] = cahe[i - 2] + cache[i - 1]
	return cache[n]
```


### Tout algo est-il "dynamisable" ?

Est-ce que  le tri fusion  a intérêt  à être traduit  selon un des  deux formats
précédents ?

## Rendu de monnaie

Formalisons un peu ce problème bien connu en introduisant la notion de contrainte.

*  Le  système  de  pièces  de  monnaie  est  un  *n*-uplet  d'entiers  naturels
  $`(p_1,p_2,...,p_n)`$, où $`p_i`$ représente la valeur de la pièce *i*


* On impose que $`p_1=1`$ afin d'avoir  toujours au moins une solution et que la
  suite **p** est strictement croissante.

* La somme à rendre est un entier naturel...

*   Une   répartition   de   pièces  est   un   *n*-uplet   d'entiers   naturels
  $`(x_1,x_2,...,x_n)`$. Il y a donc au total 
  $`\displaystyle\sum_{i=1}^nx_i`$ pièces et la somme rendue est $`\displaystyle\sum_{i=1}^nx_ip_i`$

Soit  S un  entier  naturel. On  cherche donc  un  *n*-uplet d'entiers  naturels
$`(x_1,x_2,...,x_n)`$  qui  minimise  $`\displaystyle\sum_{i=1}^nx_i`$  sous  la
contrainte $`\displaystyle\sum_{i=1}^nx_ip_i=S`$.


### Algorithme glouton

C'est souvent  celui utilisé  "dans la  vraie vie"...On  choisit la  plus "grande"
pièce possible et on  déduit sa valeur de la somme et on  recommence tant que la
somme est non nulle.

```python
from typing import Tuple, List

def glouton(S: int, P: Tuple[int]) -> Tuple[int, List[int]]:
	nb_p: int = len(P) # nb de pièces différentes disponibles
	rep: List[int] = [0]*len(P) # répartition
	nb_u: int = 0 # nb de pièces utilisées
	s: int = S # somme restante à former
	while s > 0:
		i: int = -1
		while P[i] > s:
			i -= 1
		s -= P[i]
		rep[i] += 1
		nb_u += 1
	return (nb_u, rep)
```
### Diviser permet-il de régner ?

Maintenant que vous êtes chauds, que pensez-vous de cette fonction


```python
def monnaie_div(Pieces: List[int], Somme: int) -> int:
    if Somme == 0:
        return 0
    nb_min: int = 2*Somme # nb plus grand que tous les nbs de coups possibles
    for piece in Pieces:
        if piece <= Somme:
            essai: int = 1 + monnaie_div(Pieces, Somme - piece)
            if essai < nb_min:
                nb_min = essai
    return nb_min
```


Style de programmation ? Inconvénients ? Comment y remédier ?


### Top Down


```python
def monnaie_td(Pieces: List[int], Somme: int) -> int:
    cache = [0]*(Somme + 1)
    def descente(P: List[int], S: int, C: List[int]):
        if S == 0:
            return 0
        elif cache[S] > 0: # valeur du cache de S déjà calculée
            return cache[S]
        nb_min: int = 2*S # nb plus grand que tous les nbs de coups possibles
        for piece in P:
            if piece <= S:
                essai: int = 1 + descente(P, S - piece, cache)
                if essai < nb_min:
                    nb_min = essai
                    cache[S] = nb_min
        return nb_min
    return descente(Pieces, Somme, cache)
```


Comment évolue la liste `cache` (dans quel sens est-elle remplie) ? 

### Bottom Up

Cette  fois on  construit notre  cache de  gauche à  droite, en  contruisant les
cellules des sommes en fonction des sommes inférieures.


```python
def monnaie_bu(Pieces: List[int], Somme: int) -> int:
    cache = [0]*(Somme + 1)
    for somme in range(1, Somme + 1):
        nb_min = Somme
        for piece in Pieces:
            if (piece <= somme) and (1 + cache[somme - piece]) < nb_min:
                nb_min = 1 + cache[somme - piece]
        cache[somme] = nb_min
    return cache[Somme]
```


## Sac à dos

Quelle plus  grande valeur mettre  dans le sac  quand il a un  poids max de  P et
qu'il contient n objets de valeurs $`v_i`$ et de poids $`p_i`$.

Considérons la matrice V des valeurs maxi des sacs à dos, le nomdre d'objets étant en
ligne et les poids en ordonnée. 

Ainsi $`V_{i,p}`$ est la valeur maxi d'u sace de capacité max *p* en prenant les
*i* premiers objets.

La récurrence est simple... :)

$`V_{i,p}=
\begin{cases}
0 \quad \text{ si } i = 0\\
V_{i-1, p} \quad \text{ si } p_i > p\\
\max(V_{i-1,p}, v_i+V_{i-1, p-p_i})\\
\end{cases}`$

À vous de jouer avec la chaîne de production: algo de "diviser pour régner", Top
Down, Bottom Up...


## Plus longue sous-chaîne commune


Prenons les deux chaînes:

* `X = GATACA`
* `Y = AGATEZEBLUESAVANNES`

Mettons en évidence la plus longue sous-chaîne commune, `GATAA`:


* `GATAcA`
* `aGATezebluesAvAnnes`

Soit  $`\ell_{ij}`$  la longueur  de  la  PLSC  entre  les préfixes  $`X_i`$  et
$`Y_j`$. Notons $`x_i`$ le i-ème caractère de X et $`y_j`$ le j-ème de Y. Alors:

$`\ell_{ij}=\begin{cases}
\ell_{i-1, j-1} + 1\qquad\text{ si } x_i=y_j\\
\max(\ell_{i-1, j}, \ell_{i,j-1})\qquad \text{ sinon }
\end{cases}`$


On voit poindre l'algorithme ascendant en 



```python
def souchen(chaine1:str, chaine2:str) -> int:
    l1, l2 = len(chaine1), len(chaine2)
    Tab = np.zeros((l1 + 1, l2 + 1), dtype='int')
    for i in range(1, l1 + 1):
        for j in range(1, l2 + 1):
            if chaine1[i - 1] == chaine2[j - 1]:
                Tab[i, j] = 1 + Tab[i - 1, j - 1]
            else:
                Tab[i, j] = max(Tab[i - 1, j], Tab[i, j - 1])
    return Tab
```

qui donne dans l'exemple précédent:

```python
In [2]: souchen('GATACA','AGATEZEBLUESAVANNES')
Out[2]: 
array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
       [0, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
       [0, 1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
       [0, 1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4],
       [0, 1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4],
       [0, 1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 5]])
```

Il  reste à  récupérer  cette sous-chaîne.  On peut  le  faire récursivement  en
partant du coin inférieur droit et en remontant vers la ligne de zéros.



```python
def la_souchen(chaine1:str, chaine2:str) -> str:
    tab = souchen(chaine1, chaine2)
    l1, l2 = len(chaine1), len(chaine2)
    def parcours(i,j, sc):
        if tab[i,j] == 0:
            return sc
        elif chaine1[i-1] == chaine2[j-1]:
            return parcours(i-1, j-1, chaine1[i-1] + sc)
        elif  tab[i, j - 1] > tab[i - 1, j]:
            return parcours(i, j - 1, sc)
        else:
            return parcours(i - 1, j, sc)
    return parcours(l1, l2, '')
```


