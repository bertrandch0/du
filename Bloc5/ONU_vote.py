from copy import deepcopy
from functools import reduce

f = open('vote_senateurs.txt')

l1 = list(f.read().split('\n'))

l2 = [ligne.split() for ligne in l1]

vs = {ligne[0] : [int(s) for s in ligne[3:]] for ligne in l2[:-1]}

os =  {ligne[0] : ligne[1:3] for ligne in l2[:-1]}


g = open('ONU_vote.txt')

p1 = list(g.read().split('\n'))

p2 = [ligne.split() for ligne in p1]

ps = {ligne[0] : [int(s) for s in ligne[1:]] for ligne in p2[:-1]}





def dot(xs, ys) :
    t = min(len(xs), len(ys))
    return sum([xs[i]*ys[i] for i in range(t) ])

def add(xs,ys) :
    t = min(len(xs), len(ys))
    return [xs[i] + ys[i] for i in range(t) ]

def sub(xs,ys) :
    t = min(len(xs), len(ys))
    return [xs[i] - ys[i] for i in range(t) ]

def compare(x1, x2) :
    return dot(vs[x1], vs[x2])


def plus_eloigne(x, ens) :
    ds = {(dot(vs[other], vs[x]), other) for other in (ens - {x})}
    return min(ds)

def plus_proche(x, ens) :
    ds = {(dot(vs[other], vs[x]), other) for other in (ens - {x})}
    return max(ds)


def filtre(pred, dic) :
    return tuple([x for x in dic if pred(x)])


def compare_etat(etat) :
    paire = filtre(lambda x: os[x][1] == etat, os)
    if len(paire) == 1 :
        return (46, etat, paire)
    return (dot(vs[paire[0]], vs[paire[1]]), etat, paire)

etats = {os[x][1] for x in os}

senateurs = {x for x in os}

def etats_parti(parti) :
    return {os[x][1] for x in os if os[x][0] == parti}

def senat_parti(parti) :
    return {x for x in os if os[x][0] == parti}

def senat_etat(etat) :
    return {x for x in os if os[x][1] == etat}


def moyenne_vote(ens) :
    S = reduce(add,[vs[x] for x in ens])
    D = [dot(vs[x], sub(S,vs[x])) for x in ens]
    return sum(D) / (len(ens)*(len(ens) - 1))

def moins_d_accord(ens) :
    liste = list(ens)
    n = len(liste)
    mini_score = float('inf')
    mini_sen = None
    for i in range(n - 1) :
        for j in range(i + 1, n) :
           d = dot(vs[liste[i]], vs[liste[j]])
           if d < mini_score :
               mini_score = d
               mini_sen = (liste[i], liste[j])
    return (mini_sen, mini_score)

def nb_opposants(sen, ens) :
    return sum({compare(sen, x) < 0 for x in ens})

def ens_opposants(ens) :
    return {(x,nb_opposants(x,senateurs)) for x in ens if nb_opposants(x,senateurs) > 0}

#moins d'accord : produit matriciel min tri_sup(V*tV)

# moyenne_vote([compare_etat(e) for e in etats_parti('R')])
# moyenne_vote([compare_etat(e) for e in etats_parti('D')])
