# Types Construits - Spécification

## Liste

Un type est un ensemble muni d'opérations sur cet ensemble. 

Spécifions le type `Liste`:

* une liste contient des éléments de type homogène que nous dénoterons `a`

* une  liste est soit  le vide, soit  un élément de  type `a` suivi  d'une Liste
  **constructeur**
  
* la tête d'une liste est le premier élément de la liste **sélecteur**

* la queue d'une liste est la liste qui suit la tête **sélecteur**

* la liste vide n'a ni queue ni tête


```python
#########################
##
##  1er essai de type liste
##
#############################

vide = ""

def liste(t, q):
    if q == vide:
        return str(t)
    else:
        return str(t) + liste(tete(q),queue(q))
    

def est_vide(xs):
    return xs == vide

def tete(xs):
    assert not est_vide(xs), "La liste vide n'a pas de tête"
    return xs[0]

def queue(xs):
    assert not est_vide(xs), "La liste vide n'a pas de queue"
    return xs[1:]

def affiche(xs):
    return f"{tete(xs)}|-"
    




##########################
##
## 2e essai de type liste
##
######################

class Liste_Vide:
 
    def tete():
        raise TypeError("La liste vide n'a pas de tête")

    def queue():
        raise TypeError("La liste vide n'a pas de queue")
    
    def estVide():
        return True

    def __repr__(self):
        return 'Vide'



class Liste:

    def __init__(self, tete , queue = Liste_Vide):
        """
        Une liste est définie par son premier élément et la liste des autres éléments.

        >>> xs = Liste(1,Liste(2,Liste(3,Liste_Vide)))
        
        La liste doit avoir des éléments de même type
        """
        self.__tete  = tete
        self.__queue = queue
        if (self.__tete is None):
            raise SyntaxError("la liste doit avoir la forme liste(el, liste) ou être Vide")
        if not ((self is Liste_Vide)
                or (queue is Liste_Vide)
                or isinstance(tete, type(queue.__tete))):
            raise TypeError("La liste doit être de type homogène")

    def __repr__(self):
        return f"{self.__tete}|-"

    def estVide(self):
        return (self is Vide)

    def queue(self):
        return self.__queue
    
    def tete(self):
        return self.__tete

Vide = Liste_Vide

def Est_vide(xs):
    return xs == Vide

def Tete(xs):
    return xs.tete()

def Queue(xs):
    return(xs.queue())


#############################################################
#
#
#  La barrière d'abstraction
#
##############################################################


def Longueur(xs):
    if Est_vide(xs):
        return 0
    else:
        return 1 + longueur(Queue(xs))

def Map(f, xs):
    if Est_vide(xs):
        return Liste_Vide
    else:
        return Liste(f(Tete(xs)), Map(f,Queue(xs)))

def Filtre(f, xs):
    if Est_vide(xs):
        return Liste_Vide
    elif f(Tete(xs)):
        return Liste(Tete(xs), Filtre(f, Queue(xs)))
    else:
        return Filtre(f, Queue(xs))
```

Par exemple :

```python
In [96]: xs = Liste(1,Liste(2,Liste(3,Liste_Vide)))

In [97]: Filtre(lambda x: x%2 == 0, xs)
Out[97]: 2|-

In [98]: Longueur(Filtre(lambda x: x%2 == 0, xs))
Out[98]: 1

```


Avec Haskell:

```haskell
module Liste where

infix |-

data Liste a = Vide
             | a |- (Liste a)
               deriving (Show, Eq)
             
longueur :: Liste a -> Int
longueur Vide     = 0
longueur (t |- q)  = 1 + (longueur q)
                  
applique :: (a -> a) -> Liste a -> Liste a
applique f Vide = Vide
applique f (t |- q)  = (f t) |- (applique f q) 
```

Alors:

```haskell
λ> xs = 1 |- (2 |- (3 |- Vide))
λ> longueur xs
3
λ> applique (\ x -> x*x) xs
1 |- (4 |- (9 |- Vide))
```
