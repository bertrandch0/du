from typing import List
import numpy as np

def monnaie_dyn(S, pieces):
    v = [0] + pieces
    Z = np.zeros((S+1, len(v)))
    #for i in range(1,len(v)):
    #    if v[i] <= S:
    #        Z[v[i], i] = 1
    for T in range(min(pieces),S+1):
            Z[T,1] = Z[T-v[1],1] + 1
    for i in range(2, len(v)):
        for T in range(min(pieces), S+1):
            Z[T,i] = min(Z[T, i-1], Z[max(T-v[i], 0), i] + 1)
    return Z


def monnaie_div(Pieces: List[int], Somme: int) -> int:
    if Somme == 0:
        return 0
    nb_min: int = 2*Somme # nb plus grand que tous les nbs de coups possibles
    for piece in Pieces:
        if piece <= Somme:
            essai: int = 1 + monnaie_div(Pieces, Somme - piece)
            if essai < nb_min:
                nb_min = essai
    return nb_min
    


def monnaie_td(Pieces: List[int], Somme: int) -> int:
    cache = [0]*(Somme + 1)
    def descente(P: List[int], S: int, C: List[int]):
        if S == 0:
            return 0
        elif cache[S] > 0: # valeur du cache de S déjà calculée
            return cache[S]
        nb_min: int = 2*Somme # nb plus grand que tous les nbs de coups possibles
        for piece in Pieces:
            if piece <= Somme:
                essai: int = 1 + descente(P, S - piece, cache)
                if essai < nb_min:
                    nb_min = essai
                    cache[S] = nb_min
        return nb_min
    return descente(Pieces, Somme, cache)


########################
#
#  PLUS LONGUE SOUS-CHAINE
#
########################


def souchen(chaine1:str, chaine2:str) -> int:
    l1, l2 = len(chaine1), len(chaine2)
    Tab = np.zeros((l1 + 1, l2 + 1),dtype='int')
    sc = ""
    for i in range(1, l1 + 1):
        for j in range(1, l2 + 1):
            if chaine1[i - 1] == chaine2[j - 1]:
                Tab[i, j] = 1 + Tab[i - 1, j - 1]
            else:
                Tab[i, j] = max(Tab[i - 1, j], Tab[i, j - 1])
    return Tab


def la_souchen(chaine1:str, chaine2:str) -> str:
    tab = souchen(chaine1, chaine2)
    l1, l2 = len(chaine1), len(chaine2)
    def parcours(i,j, sc):
        if tab[i,j] == 0:
            return sc
        elif chaine1[i-1] == chaine2[j-1]:
            return parcours(i-1, j-1, chaine1[i-1] + sc)
        elif  tab[i, j - 1] > tab[i - 1, j]:
            return parcours(i, j - 1, sc)
        else:
            return parcours(i - 1, j, sc)
    return parcours(l1, l2, '')
            
            
