## Ça manque d'explication et de typage...N'hésitez pas à améliorer !!


class Noeud:
    """Un noeud a une valeur et pointe vers un autre noeud"""

    def __init__(self, elt = None, sui = None):
        self.__valeur  = elt
        self.__suivant = sui

    def get_valeur(self):
        """accesseur"""
        return self.__valeur
    
    def set_valeur(self,v):
        """mutateur"""
        self.__valeur = v

    def get_suivant(self):
        return self.__suivant

    def set_suivant(self,s):
        self.__suivant = s

    def  __str__(self):
        """ on affiche seulement la valeur du noeud """
        return str(self.get_valeur())
    

class Pile:
    
    def __init__(self):
        self.__tete = None
        
    def est_vide(self):
        return self.__tete is None

    def empile(self,elt):
        """La tête a pour valeur la valeur entrée et pointe vers l'ancienne tête"""
        n = Noeud(elt)
        n.set_suivant(self.__tete)
        self.__tete = n

    def val_tete(self):
        return self.__tete.get_valeur()
        
    def depile(self):
        """On enlève la tête de la pile et on retourne la valeur de la tete"""
        assert not self.est_vide(), "La pile vide ne peut pas être dépilée"
        t = self.__tete
        v = self.val_tete()
        self.__tete = t.get_suivant()
        return v

    def __repr__(self):
        return f"{self.__tete.get_valeur()}|-"
    
