##################################################################################################
##### CREATION ET RESOLUTION D'UN LABYRINTHE PARFAIT PAR DEUX METHODES DONT "A MAIN DROITE" ######
##################################################################################################

##############################################################
#################### Modules importés  #######################

import numpy as np
from random import*
from copy import deepcopy
import matplotlib as mpl
import matplotlib.pyplot as plt
from time import time

###############################################################
############  Déclaration des variables globales  #############

CM = mpl.colors.ListedColormap(['red', 'lightblue', 'white', 'grey','sienna',
'lightgreen'])
#Couleurs personnalisées [-2,-1,0,1,2,3]
#bord:-2 Mur:1  case vide:0  chemins de résolution en profondeur/à main droite:3     
bord=-2     #bord du plateau que l'on ne doit pas atteindre
n=101       #n doit être un nombre impair, la taille du labyrinthe est (n-1)/2

###############################################################
################  Création du labyrinthe    ###################
###############################################################

#Fonctions secondaires de création du labyrinthe
def initialise_plateau(n):
    """Fonction qui initialise le quadrillage de murs: 0=chemin et 1=mur et les bords"""  
    plateau = np.zeros((n,n))  
    for i in range(1,n-1):
        for j in range(2,n-1,2):
            plateau[i][j] = 1           #On construit un mur
    for i in range(2,n-1,2):
        for j in range(1,n-1):
            plateau[i][j]=1             #On construit un mur
    for i in range(n):
        plateau[0][i]= bord             #On construit un bord
        plateau[n-1][i]= bord
        plateau[i][0] = bord
        plateau[i][n-1] = bord     
    return plateau

def nombre_cases(n,plateau1):
    """Fonction qui détermine le nombre de cases à visiter du labyrinthe"""  
    nb_case=0                #nombre total des cases à visiter du labyrinthe
    for i in range(1,n-1):  #trouver le nombre de cases à visiter
        for j in range(1,n-1):
            if plateau1[i,j]==0:
                nb_case+=1             
    return nb_case
    
def candidates(i,j,CV):
    """Fonction qui détermine les cases voisines non visitées de la case [i,j], soit les cases voisines candidates pour poursuivre le chemin"""
    
    vois_theo=[[i-2,j],[i+2,j],[i,j-2],[i,j+2]] #cases voisines de la case [i,j]
    vois_reel=[] #liste des cases voisines non visitées
    for [k,l] in vois_theo:
        if  k in range(1,n-1) and l in range(1,n-1) and [k,l] not in CV:
        #si la case n'est pas sur un bord et est non visitée
            vois_reel.append([k,l])             
            #On l'ajoute à la liste des cases voisines non visitées 
            
    return vois_reel

#Fonction principale de création du labyrinthe

def cree_labyrinthe():
    """Fonction qui crée un labyrinthe carré parfait de taille n x n"""  
    i,j=1,1                         #choix de la case de départ: entrée 
    plateau1 =initialise_plateau(n) #initialisation du quadrillage de murs
    cases_vues=[[i,j]]              #liste des cases visitées
    nb_case_a_visiter=nombre_cases(n,plateau1)#on doit visiter les cases vides   
    while len(cases_vues)!=nb_case_a_visiter :      
    #tant qu'on a pas visité toutes les cases vides du labyrinthe
        cases_candidates=candidates(i,j,cases_vues) 
        #liste des cases candidates pour poursuivre le chemin                 
        if cases_candidates != []:
            prochaine_case=choice(cases_candidates)
            #choix aléatoire de la case qui poursuit le chemin parmiles cases candidates        
            a,b=prochaine_case[0],prochaine_case[1] 
            #coordonnées de la case qui poursuit le chemin
            x_mur,y_mur=(i+a)//2,(j+b)//2
            plateau1[x_mur,y_mur]=0   
            #on casse le mur entre la case actuelle et celle qui poursuit le chemin      
        else:        
        #Si on se trouve dans une impasse
            impasse = len(cases_vues) -1 
            #position de la dernière case visitée avant l'impasse
            recul= 0                #nombre de cases dont on doit reculer
            while cases_candidates == []: 
            #tant qu'il n'y a pas de case non visitée autour de la case actuelle
                recul += 1          #on recule d'une case de plus
                x,y = cases_vues[impasse - recul][0], cases_vues[impasse - recul][1]    
                #coordonnées de la case où l'ont a reculé
                cases_candidates = candidates(x,y,cases_vues)                           
                #liste des cases candidates pour poursuivre le chemin 
            prochaine_case=choice(cases_candidates)#case poursuivant le chemin
            a,b= prochaine_case[0],prochaine_case[1] 
            #coordonnées de la case qui poursuit le chemin
            x_mur,y_mur=(x+a)//2,(y+b)//2
            plateau1[x_mur,y_mur]=0 
            #on casse le mur entre case actuelle et celle poursuivant chemin            
        cases_vues.append([a,b])    
        #on ajoute la case qui poursuit le chemin à la liste des cases visitées
        i,j=a,b                     #nouvelles coordonnées de départ 
    return [plateau1,x] 


###############################################################
################  RESOLUTION EN PROFONDEUR  ###################
###############################################################

#Fonctions secondaires de resolution en profondeur

def voisinage(i,j):
    """renvoie la liste des voisins théoriques de la case de coordonnées [i,j]""" 
    V= [[i,j-1],[i+1,j],[i, j+1],[i-1,j]]  
    return V

#Fonction principale
def resolution_profondeur(Y,plateau2):           
    """Fonction qui cherche un chemin de sortie. Le labyrinthe est parfait, on
est sûr d'y arriver. L'idée est d'aller le plus loin possible dans 
l'exploration, on visite prioritairement les voisins d'une case les plus 
récemment découverts. Y: coordonnées de la case de sortie ; plateau2: plateau1"""
    if plateau2[Y[0],Y[1]]!=0: 
    #si la case de sortie est un mur ou un bord: case inaccessible
        print("case", Y," inaccessible, c'est un mur ou un bord")
        return 
    parents=[[1,1]]  #liste des parents des cases du chemin de résolution
    chemin=[[1,1]]                           
    while  True:   #le labyrinthe est parfait, on est sûr de finir par sortir
        s=parents[-1]                                 
        voisins= voisinage(s[0],s[1])#cases voisines de celle où on se trouve
        vois=[]  #liste des cases voisines non visitées
        for vo in voisins:                      
            if vo not in chemin:     
            #si la case candidate ne fait pas déjà partie du chemin          
                x,y=vo
                if x in range(n) and y in range(n): 
                #si la case candidate n'est pas un bord
                    if plateau1[x,y]==0 :     
                    #si la case candidate n'est pas un mur
                        vois.append(vo) 
                        #on l'ajoute à la liste des cases candidates                
        if Y in vois:                           
        #si la sortie se trouve dans les cases voisines à la case actuelle
            chemin.append(Y)                    
            #on va directement à la sortie 
            return chemin          
        elif len(vois)!=0:                      
            news=choice(vois)                  
            #choix aléatoire d'une des cases candidates pour poursuivre chemin 
            parents.append(news)
            chemin.append(news)
        else:         
        #si aucune case voisine n'est candidate pour poursuivre le chemin
            parents.pop()           #on revient en arrière
            chemin.append(parents[-1])               
            
###############################################################
#############  RESOLUTION " A MAIN DROITE"  ###################
###############################################################
#Fonctions secondaires
def voisin_main_droite(orient,i,j):
    """ Renvoie la liste des cases voisines d'une case, dans l'ordre des cases 
les plus à droite de la case actuelle:
- Si orient==0 c'est qu'on regarde vers le Nord. 
Ordre de voisins [[i,j+1],[i-1,j],[i,j-1],[i+1,j]]
- Si orient==1 on regarde vers l'ouest : voisins [[i-1,j],[i,j-1],[i+1,j],[i,j+1]]
- Si orient==2 on regarde vers le sud. Voisins [[i,j-1],[i+1,j],[i,j+1],[i-1,j]]
- Si orient==3 on regarde vers l'est. Voisins [[i+1,j],[i,j+1],[i-1,j],[i,j-1]]"""   
    if orient==0: #nord
        vtheo= [[i,j+1],[i-1,j],[i,j-1],[i+1,j]]
        direction=[3,0,1,2]
    elif orient==1: #ouest
        vtheo=[[i-1,j],[i,j-1],[i+1,j],[i,j+1]]
        direction=[0,1,2,3]
    elif orient==2: #sud
        vtheo=[[i,j-1],[i+1,j],[i,j+1],[i-1,j]]
        direction=[1,2,3,0]
    else: #est
        vtheo=[[i+1,j],[i,j+1],[i-1,j],[i,j-1]]
        direction=[2,3,0,1]  
    return [vtheo,direction]

#Fonction principale
def trouve_chemin_main_droite(plateau2):
    """Fonction qui élabore un chemin de résolution selon la méthode 
"à main droite": on imagine un personnage entrant dans le labyrinte en case
 ([1,1]) et suivant toujours le mur qui est à sa droite jusqu'à trouver la
 sortie ([n-2,n-2])"""  
    chemin=[[1,1]]  #liste des coordonnées des cases du chemin
    sortie=[n-2,n-2]
    orient=3  #on regarde vers l'est en entrant dans le labyrinthe en case [1,1] 
    while sortie not in chemin:   
    #tant qu'on a pas trouvé la sortie
        i,j=chemin[-1]                      
        voisins,direct=voisin_main_droite(orient,i,j) 
        #recherche des cases voisines dans l'ordre de la plus à droite     
        for k in range(4):
            x,y=voisins[k]      
            if plateau2[x,y]==0:                  
            #si la case est vide (ni un bord ni un mur)
                orient=direct[k] #on regarde maintenant dans la direction direct[k]
                chemin.append(voisins[k])  
                #le chemin est poursuivi par la case de coordonnées données par voisins[k]
                break             
    return chemin

###############################
## Autres fonctions communes ##
###############################
def compresse_chemin(L):  
    """Fonction qui compresse le chemin de résolution pour obtenir
le chemin le plus court, en enlevant les passages par impasses""" 
    n=len(L)                    #L: liste des coordonnées des cases parcourues
    L2=L[:]                     #copie de la liste L
    for i in range(n):    
        for j in range(n-1,i,-1):       
            if L[i]==L[j]:  
            #si on a visité deux fois une case: présence d'un cul de sac à éliminer
                for k in range(i,j):
                    L2[k]=0                     
                    #on élimine le cul de sac en attribuant à ses cases la valeur 0
    #Reconstruction du chemin de résolution à partir de L2                           
    L3=[]                                      
    for elt in L2:
        if elt!=0:
            L3.append(elt)
    return L3

def trace_chemin(chemin):
    """"Fonction qui représente le chemin de résolution parcouru"""   
    plateau=deepcopy(plateau1)
    for k in range(0,len(chemin)):
        plateau[chemin[k][0],chemin[k][1]]=3 #le chemin apparait d'une autre couleur       
    return plateau

##############################################################
## Construction d'un labyrinthe et constructions de chemins ##
##############################################################
#Introduction
print("On souhaite créer et résoudre un labyrinthe parfait")
print ("Pour la création, on fait une recherche exhaustive -->Wikipedia")
print("-"*50)
print ("Pour la résolution on fait une recherche en profondeur,")
print( "puis une recherche en allant le plus à droite possible")
print("qui est grosso modo une variante orientée du parcours en profondeur")
print("-"*50)
print("Comme il y  beaucoup d'impasses dans la recherche, on compresse")
print("ensuite le chemin...")
print("-"*50)
print("Patientez quelques secondes, on construit et résout un labyrinthe de taille ",n,"...")
#Construction du labyrinthe par exploration exhaustive
plateau1=cree_labyrinthe()[0]
copie_plateau1=deepcopy(plateau1)     #copie de sauvegarde pour besoins ultérieurs
plt.figure("Labyrinthe initial")
plt.imshow(plateau1,cmap=CM,vmin=-2,vmax=3,interpolation="none")

sortie=[n-2,n-2]#Sortie arbitraire, attention à ne pas choisir un mur!!!

#Construction d'un chemin de sortie par un parcours en profondeur
CV= resolution_profondeur(sortie,plateau1)
#print("Chemin trouvé par recherche en profondeur",CV)
plt.figure("Solution en profondeur non optimisée")
plt.imshow(trace_chemin(CV),cmap=CM,vmin=-2,vmax=3,interpolation="none")

Compress=compresse_chemin(CV)
#print("Chemin compressé, méthode profondeur: ",Compress)
plt.figure("Solution en profondeur optimisée")
plt.imshow(trace_chemin(Compress),cmap=CM,vmin=-2,vmax=3,interpolation="none")   

#Construction d'un chemin de sortie par un parcours main droite
L=trouve_chemin_main_droite(plateau1)
plt.figure("Solution main droite non optimisée")
plt.imshow(trace_chemin(L),cmap=CM,vmin=-2,vmax=3,interpolation="none")

Compress2=compresse_chemin(L)
#print("Chemin compressé, méthode main droite: ",Compress2)
plt.figure("Solution main droite  optimisée")
plt.imshow(trace_chemin(Compress2),cmap=CM,vmin=-2,vmax=3,interpolation="none")

print("Normalement vous devriez voir s'afficher 5 figures...")
print("Puis quand vous fermerez les figures, il y aura une suite...")
plt.show()#affichage de toutes les figures

################################################################
######### COMPARAISON DES DEUX METHODES DE RESOLUTION ##########
################################################################
print("-"*50)
print("Ne quittez pas! Maintenant on essaie d'évaluer la méthode la plus rapide...")

#temps de résolution par la méthode "à main droite".
#On ne fait qu'un essai car pas d'aléatoire
x,y = 0,0
L=[]
temps, temps_moy = 0, 0

x= time()
L=trouve_chemin_main_droite(plateau1)
y= time()
temps += y-x
temps_moy= temps
print("Temps de résolution par la méthode 'à main droite': ",temps_moy)

#temps de résolution par la méthode "en profondeur".
#On fait une moyenne car ici il y a un choix aléatoire.
x,y = 0,0
L=[]
temps, temps_moy = 0, 0
for k in range(10):
    x= time()
    L=resolution_profondeur(sortie,plateau1)
    y= time()
    temps += y-x
temps_moy= temps/10
print("Temps de résolution par la méthode 'en profondeur': ",temps_moy)

        



