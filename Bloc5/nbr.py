from typing import Optional, TypeVar, Any, List, Generic
from typing_extensions import Protocol
from abc import abstractmethod
import subprocess

# On crée un type générique d'objets comparables
# On ne peut en effet créer d'ABR qu'avec des valeurs
# comparables
C = TypeVar("C", bound="Comparable")


class Comparable(Protocol):
    """
    Un protocole est un type abstrait dont les sous-types ont la même
    structure.
    Ici, on crée un type d'objets comparables : les objets doivent
    pouvoir être comparés avec les méthodes ==, < et >
    """
    @abstractmethod
    def __eq__(self, other: Any) -> bool:
        pass

    @abstractmethod
    def __lt__(self: C, other: C) -> bool:
        pass

    def __gt__(self: C, other: C) -> bool:
        return (not self < other) and self != other


class Noeud(Generic[C]):
    """
    Un noeud a une valeur et pointe vers deux autres noeuds (petit et
    grand) ou éventuellement le vide.
    On insère de nouvelles valeurs en partant de la racine du noeud et
    en bifurquant selon la comparaison avec la valeur du noeud.

    """

    def __init__(self: 'Noeud[C]', val: C) -> None:
        """
        Un noeud a toujours une valeur mais pointe vers un autre noeud
        ou éventuellement le vide (None)
        """
        self.__val: C = val
        self.__grand: Optional['Noeud[C]'] = None
        self.__petit: Optional['Noeud[C]'] = None

    def insere(self: 'Noeud[C]', val: C) -> None:
        """
        Permet d'insérer une valeur comparable dans un noeud.
        """
        if val < self.__val:
            if self.__petit:
                self.__petit.insere(val)
            else:
                self.__petit = Noeud(val)
        elif val > self.__val:
            if self.__grand:
                self.__grand.insere(val)
            else:
                self.__grand = Noeud(val)
        else:
            print(f'{val} est un doublon')

    def hauteur(self: 'Noeud[C]') -> int:
        """
        Nombre de niveaux de l'arbre
        """
        hb: int = self.__grand.hauteur() if self.__grand else 0
        hh: int = self.__petit.hauteur() if self.__petit else 0
        return 1 + max(hb, hh)

    def nb_noeuds(self: 'Noeud[C]') -> int:
        nb: int = self.__grand.nb_noeuds() if self.__grand else 0
        nh: int = self.__petit.nb_noeuds() if self.__petit else 0
        return 1 + nb + nh

    def est_feuille(self: 'Noeud[C]') -> bool:
        return not (self.__grand or self.__petit)

    def est_unaire(self: 'Noeud[C]') -> bool:
        return not ((self.__grand and self.__petit) or self.est_feuille())

    def nb_feuilles(self: 'Noeud[C]') -> int:
        if self.est_feuille():
            return 1
        elif self.__petit and not self.__grand:
            return self.__petit.nb_feuilles()
        elif self.__grand and not self.__petit:
            return self.__grand.nb_feuilles()
        elif self.__grand and self.__petit:
            return self.__grand.nb_feuilles() + self.__petit.nb_feuilles()
        else:
            return 0

    def contient(self: 'Noeud[C]', v: C, cpt=0) -> bool:
        val: C = self.__val
        if v == val:
            #print(f'En {cpt+1} comparaisons')
            return True
        elif v > val and self.__grand:
            return self.__grand.contient(v, cpt+1)
        elif v < val and self.__petit:
            return self.__petit.contient(v, cpt+1)
        else:
            #print(f'En {cpt+1} comparaisons')
            return False

    def visite_pre(self: 'Noeud[C]') -> None:
        print(self.__val)
        for cote in [self.__petit, self.__grand]:
            if cote:
                cote.visite_pre()

    def visite_post(self: 'Noeud[C]') -> None:
        for cote in [self.__petit, self.__grand]:
            if cote:
                cote.visite_post()
        print(self.__val)

    def visite_inf(self: 'Noeud[C]') -> None:
        if self.__petit:
            self.__petit.visite_inf()
        print(self.__val)
        if self.__grand:
            self.__grand.visite_inf()

    def visite_niveau(self: 'Noeud[C]') -> None:
        a_visiter: List['Noeud[C]'] = [self]
        while a_visiter != []:
            noeud: 'Noeud[C]' = a_visiter[0]
            print(noeud.__val)
            a_visiter = a_visiter[1:]
            if noeud.__petit:
                a_visiter.append(noeud.__petit)
            if noeud.__grand:
                a_visiter.append(noeud.__grand)

    def mini(self: 'Noeud[C]') -> C:
        if self.__petit is None:
            return self.__val
        else:
            return self.__petit.mini()
        
    def maxi(self: 'Noeud[C]') -> C:
        if self.__grand is None:
            return self.__val
        else:
            return self.__grand.maxi()
        
    #  Outils de représentation
    
    def tree2viz(self: 'Noeud[C]') -> str:
        """
        Utilise graphviz pour représenter l'arbre.
        Au besoin, l'installer:
          $ sudo apt-get install graphviz
        Chaque arête est représentée sous la forme:
        père -> fils;
        """
        s = ''
        v = self.__val
        if self.__petit and self.__grand:
            s += f'{self.__petit.__val}[style=filled,fillcolor=cadetblue3];\n'
            s += f'{self.__grand.__val}[style=filled,fillcolor=darkorange];\n'
            s += f'{v}->{self.__petit.__val};\n'
            s += f'{v}->{self.__grand.__val};\n'
            s += self.__petit.tree2viz()
            s += self.__grand.tree2viz()
        elif self.__petit and not self.__grand:
            s += f'{self.__petit.__val}[style=filled,fillcolor=cadetblue3];\n'
            s += f'{v}->{self.__petit.__val};\n'
            s += f'nullp{v}[shape=point];\n{v}->nullp{v};\n'
            s += self.__petit.tree2viz()
        elif self.__grand and not self.__petit:
            s += f'{self.__grand.__val}[style=filled,fillcolor=darkorange];\n'
            s += f'nullg{v}[shape=point];\n{v}->nullg{v};\n'
            s += f'{v}->{self.__grand.__val};\n'
            s += self.__grand.tree2viz()
        else:
            s += f'nullg{v}[shape=point];\n{v}->nullg{v};\n'
            s += f'nullp{v}[shape=point];\n{v}->nullp{v};\n'
        return s

    def affiche(self: 'Noeud[C]') -> None:
        """
        Finit de mettre au format dot de graphviz :
        digraph G{
           s1 -> s2;
           s1 -> s3;
        }
        puis exporte au format png et affiche le résultat
        avec l'outil de visualisation par défaut
        """
        nl = '\n'
        s = f'digraph G{{{nl} graph [ordering="out"];{nl}{self.tree2viz()}}}'
        with open('arbre.dot', 'w') as f:
            f.write(s)
        c = 'dot -Tpng arbre.dot -o arbre.png && xdg-open arbre.png'
        subprocess.call(c, shell=True)

    def __repr__(self: 'Noeud[C]') -> str:
        """
        Utilise graph-easy pour afficher les arbres en mode texte.
        Au besoin, installer graph-easy:
          $ sudo apt install cpanminus
          $ sudo cpanm Graph::Easy
        Ensuite on récupère la sortie standard qui est au format binaire
        et on l'encode en utf8 pour l'avoir au format str exigé par repr
        """
        nl = '\n'
        s = f'digraph G{{{nl}graph [ordering="out"];{nl}{self.tree2viz()}}}'
        with open('arbre.dot', 'w') as f:
            f.write(s)
        cc = 'graph-easy --from=dot --as_ascii arbre.dot'.split()
        res = subprocess.run(cc, stdout=subprocess.PIPE)
        return res.stdout.decode('utf-8')
