from nbr import *

class Arbre(Generic[C]):
    """
    Arbre binaire de recherche constitué de noeuds.
    Reprend les méthodes de la classe Noeud en incluant le cas vide
    et en construisant un arbre à partir d'un noeud.
    """

    def __init__(self: 'Arbre[C]') -> None:
        """
        Constructeur : un arbre est vide ou constitué de noeuds
        """
        self.__data: Optional[Noeud[C]] = None

    def est_vide(self: 'Arbre[C]') -> bool:
        """
        Testeur : vérifie si un arbre est vide
        """
        return self.__data is None

    def insere(self: 'Arbre[C]', val: C) -> None:
        """
        Insère un élément comparable dans un arbre selon le critère
        choisi pour les noeuds.
        Si l'arbre est vide, crée le noeud-data
        """
        if self.__data is None:
            self.__data = Noeud(val)
        else:
            self.__data.insere(val)

    def hauteur(self: 'Arbre[C]') -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.hauteur()

    def nb_noeuds(self: 'Arbre[C]') -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.nb_noeuds()

    def est_feuille(self: 'Arbre[C]') -> bool:
        if self.__data is None:
            return False
        else:
            return self.__data.est_feuille()

    def nb_feuilles(self: 'Arbre[C]') -> int:
        if self.__data is None:
            return 0
        else:
            return self.__data.nb_feuilles()

    def contient(self: 'Arbre[C]', v: C) -> bool:
        if self.__data is None:
            return False
        return self.__data.contient(v)

    def visite_pre(self: 'Arbre[C]') -> None:
        if self.__data:
            self.__data.visite_pre()

    def visite_post(self: 'Arbre[C]') -> None:
        if self.__data:
            self.__data.visite_post()

    def visite_inf(self: 'Arbre[C]') -> None:
        if self.__data:
            self.__data.visite_inf()

    def visite_inf_imp(self: 'Arbre[C]') -> None:
        if self.__data:
            self.__data.visite_inf_imp()

    def visite_niveau(self:'Arbre[C]') -> None:
        if self.__data:
            self.__data.visite_niveau()

    def mini(self: 'Arbre[C]') -> C:
        assert self.__data, 'Arbre vide ! Pas de minimum'
        return self.__data.mini()

    def maxi(self: 'Arbre[C]') -> C:
        assert self.__data, 'Arbre vide ! Pas de maximum'
        return self.__data.maxi()

    def affiche(self: 'Arbre[C]') -> None:
        assert self.__data, 'Arbre vide'
        self.__data.affiche()

    def __repr__(self: 'Arbre[C]') -> str:
        if self.__data is None:
            return 'Arbre_Vide'
        else:
            return self.__data.__repr__()
