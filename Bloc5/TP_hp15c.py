import pile as p
import operator

operations = {  # dict d'opérations pour les utiliser en position préfixe 
    '+' : operator.add, '-'  :  operator.sub,
    '*' : operator.mul, '%'  :  operator.mod,
    '**': operator.pow,  '//': operator.floordiv,
    '/' : operator.truediv
}


def mon_hp(chaine : str) -> int:
    les_ops = chaine.split(' ')  # les_ops est une Liste de string
    mon_stock = p.Pile()  # pour stocker les entiers
    for op in les_ops:  # chaque op est de type string
        print(mon_stock)
        if op in operations:
            operateur = operations[op]  # une fonction (nb,nb) → nb
            operande2 = mon_stock.depile()  # le 1er nb lu est le dernier entré
            operande1 = mon_stock.depile()  # le 2e nb lu est le 1er entré
            mon_stock.empile(operateur(operande1, operande2))
        else:  # si pas opérateur alors c'est une chaîne représentant un entier
            mon_stock.empile(int(op))  # je stocke l'entier correspondant à la chaîne
    resultat = mon_stock.depile()  # il ne doit rester qu'1 nb dans mon stock
    assert mon_stock.est_vide(), "Expression mal formée"  # sinon il y a eu un pb
    return resultat
