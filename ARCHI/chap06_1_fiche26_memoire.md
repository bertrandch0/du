% Mémoire et langage machine

### En Bref : 
En son coeur, la machine effectue des calculs à partir d'instructions simples et
chiffrées
qui peuvent être directement traduites dans un langage rudimentaire (assembleur)
qui prend, traite et remplit des cases  mémoires dont les rôles, les vitesses et
les tailles sont différents.

## Organisation de la mémoire

Il existe de nombreux mécanismes de mémoire qui se distinguent par leur
coût, leur vitesse, leur durabilité (volatile ou permanente), leur mode d'accès
(par adresse  ou dans l'ordre  de leur rangement).  En règle générale,  plus une
mémoire est efficace, plus elle est chère. 

On parle de *mémoire vive* quand le
contenu est  perdu lorsque  le courant  s'arrête: il  s'agit des  registres, des
mémoires cache, de la mémoire centrale.

Les autres mémoires sont persistantes: les  SSD (Solid State Drive), les disques
magnétiques.

Il existe une troisième catégorie, à part, la ROM (Read Only Memory) qui, comme
son nom l'indique, ne fonctionne (principalement) qu'en lecture seule.


### Registres

Un registre  est un emplacement mémoire  interne au processeur pour  stocker des
opérandes et des résultats intermédiaires lors  des opérations
effectuées  dans l'UAL  notamment. Leur  capacité,  leur nombre  et leurs  rôles
varient selon les processeurs. La plupart des PC actuels ont des registres de
taille 64 bits.  Ils sont accessibles via le jeu  d'instructions (cf section Jeu
d'instructions).

### Mémoire centrale

C'est  une mémoire  vive qui  contient les  programmes en  cours et  les données
qu'ils  manipulent. Elle  est  de  taille importante  (plusieurs  Go). Elle  est
organisée  en cellules  qui contiennent  chacune une  donnée ou  une instruction
repérées par un entier (une adresse  mémoire). Le temps d'accès à chaque cellule
est le  même: on  parle donc  de mémoire  accès aléatoire  (RAM :  Random Access
Memory) mais il est plus précis de parler de mémoire à accès direct.



### Mémoires cache

Pour pouvoir adapter la très grande vitesse du processeur à celle très
faible (relativement) de la mémoire centrale, on place entre eux une mémoire plus
rapide: la  *mémoire cache* qui  contient les  instructions et données  en cours
d'utilisation car la plupart du temps, les données qui viennent d'être utilisées
ont une  probabilité plus grande d'être  réutilisées que d'autres. Il  s'agit le
plus souvent  de mémoires de  type RAM statiques  (SRAM) plus rapides  mais plus
chères que les  mémoires de type RAM dynamiques (SDRAM,  DDR,...) utilisées dans
la mémoire centrale.


## Jeu d'instructions


Un programme écrit  dans un langage de  haut niveau dépend le  moins possible du
processeur et du système d'exploitation  utilisés.  Mais chaque processeur a son
langage. Il existe  une chaîne de production permettant de  passer du langage de
haut  niveau  au langage  machine  écrit  en  binaire  correspondant à  un  *jeu
d'instructions* spécifique. Ces jeux ont toutefois des structures communes.

Chaque instruction contient  un code correspondant à l'opération  à effectuer et
aux opérandes. Une opération peut être:

* une opération de transfert entre les registres et la mémoire par exemple ;
* une opération de calcul arithmétique ou logique: addition, comparaison,...
*  un branchement   via des  *sauts* vers  une
  certaine  adresse selon  le  résultat de  l'opération précédente  (branchement
  conditionnel) ou non (inconditionnel).
  
Il est parfois utile  de programmer au plus bas de la  machine mais peu pratique
de  le faire  en  binaire. Il  existe  donc un  petit  programme de  traduction,
l'*assembleur*, qui permet de passer directement du langage machine à un langage
dit d'*assemblage* plus lisible pour l'être humain:

* le jeu d'instruction est exactement identique à celui du langage machine;
* les  opérations sont désignés  par des  *mnémoniques* comme par  exemple `add`
  (addition), `lw` (charge mot *load word*), `sw` (range mot *store word*), etc.
* les registres ont des noms: `$t0`, `$t1`, `ax`, etc.
*  les constantes  sont exprimables  en hexadécimal,  binaire, etc.:  `0x26a3f`,
  `0b0011001`,... 
* les branchements se font via  des étiquettes: `bne $t1 $t0 _plusLoin` signifie
  que si les contenus des registres `$t1` et `$t0` sont différents alors il faut
  aller à la ligne repérée par `_plusLoin`.
  
Par exemple, voici une instruction  dans le langage d'assemblage d'un processeur
MIPS 32 bits si l'on veut additionner dans le registre `$t0` la somme des valeurs
contenues dans les registres `$a0` et `$v0`: `add $a0 $v0 $t0` qui sera traduite
en langage machine par `0b00000000100000100100000000100000`.


